import execa from "execa";

export async function startProject(options) {
  const pwa = await execa("npm", ["run", "devlocal"], {
    cwd: process.cwd() + "/" + options.projectName
  });

  if (pwa.failed) {
    return Promise.reject(new Error("Failed to clone repository"));
  }

  const graphql = await execa("npm", ["run", "dev"], {
    cwd: process.cwd() + "/" + options.graphqlName
  });

  if (pwa.failed) {
    return Promise.reject(new Error("Failed to clone repository"));
  }

  const browser = await execa("open", ["http://localhost"]);

  if (browser.failed) {
    return Promise.reject(new Error("Failed to clone repository"));
  }
  return;
}
