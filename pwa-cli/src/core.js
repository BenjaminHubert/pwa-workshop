import execa from "execa";

export async function addCore(options) {
  const installPath = process.cwd() + "/" + options.projectName;

  const remoteAdd = await execa(
    "git",
    [
      "remote",
      "add",
      "core",
      "git@gitlab.com:unifygroup/aufeminin/dev/pwa-nuxt-core.git"
    ],
    {
      cwd: installPath
    }
  );
  if (remoteAdd.failed) {
    return Promise.reject(new Error("Failed to add git remote"));
  }

  const fetchCore = await execa("git", ["fetch", "core"], {
    cwd: installPath
  });
  if (fetchCore.failed) {
    return Promise.reject(new Error("Failed to fetch core"));
  }

  const readTree = await execa(
    "git",
    ["read-tree", "--prefix=core", "-u", "core/test"],
    {
      cwd: installPath
    }
  );
  if (readTree.failed) {
    return Promise.reject(new Error("Failed to read-tree"));
  }

  const commitCore = await execa("git", ["commit", "-m", "Add core subtree"], {
    cwd: installPath
  });
  if (commitCore.failed) {
    return Promise.reject(new Error("Failed to commit core"));
  }

  if (options.createGitlabRepo) {
    // const pushGitlab = await execa("git", ["push"], {
    //   cwd: installPath
    // });
    // if (pushGitlab.failed) {
    //   return Promise.reject(new Error("Failed to push core"));
    // }
    //git remote set-url origin git@github.com:USERNAME/REPOSITORY.git
    // const pushGitlab = await execa("git", ["push"], {
    //   cwd: installPath
    // });
    // if (pushGitlab.failed) {
    //   return Promise.reject(new Error("Failed to push core"));
    // }
  } else {
  }

  return;
}

const fs = require("fs");

function fileExist(filePath) {
  return new Promise((resolve, reject) => {
    fs.access(filePath, fs.F_OK, err => {
      if (err) {
        console.error(err);
        return reject(err);
      }
      //file exists
      resolve();
    });
  });
}

export async function updateCore(options) {
  const path = process.cwd() + "/core";

  await fileExist(path);

  const fetchCore = await execa("git", ["fetch", "core"]);
  if (fetchCore.failed) {
    return Promise.reject(new Error("Failed to fetch core"));
  }

  const updateCore = await execa("git", [
    "merge",
    "-s",
    "subtree",
    "--squash",
    "core/test",
    "--allow-unrelated-histories"
  ]);
  if (updateCore.failed) {
    return Promise.reject(new Error("Failed to update core"));
  }

  const commitCore = await execa("git", ["commit", "-m", "Update core"]);
  if (commitCore.failed) {
    return Promise.reject(new Error("Failed to fetch core"));
  }

  return;
}
