import execa from "execa";

export async function cloneRepo(options) {
  const installPath = process.cwd() + "/" + options.projectName;

  const result = await execa("git", [
    "clone",
    "-b",
    "barcelone-workshop",
    "git@gitlab.com:unifygroup/aufeminin/dev/pwa-nuxt-template.git",
    options.projectName
  ]);

  if (result.failed) {
    return Promise.reject(new Error("Failed to clone repository"));
  }

  if (!options.createGitlabRepo) {
    //remove remote
    const result = await execa("git", ["remote", "rm", "origin"], {
      cwd: installPath
    });

    if (result.failed) {
      return Promise.reject(new Error("Failed to remove remote origin"));
    }
  }
  return;
}
