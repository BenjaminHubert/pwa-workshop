import arg from "arg";
import inquirer from "inquirer";
import { createProject, coreUpdate } from "./main";

function parseArgumentsIntoOptions(rawArgs) {
  const args = arg(
    {
      "--name": String
    },
    {
      create: String
    },
    {
      argv: rawArgs.slice(2)
    }
  );
  return {
    projectName: args["--name"],
    action: args._[0]
  };
}

async function promptForMissingOptions(options) {
  const defaultName = {
    projectName: "pwa-project"
  };

  if (options.skipPrompts) {
    return {
      ...options,
      template: options.template || defaultName
    };
  }
  const questions = [];

  if (!options.action) {
    questions.push({
      type: "list",
      name: "action",
      choices: ["create", "core-update"],
      default: "create"
    });
  }

  if (!options.projectName) {
    questions.push({
      type: "input",
      name: "projectName",
      message: "project name",
      default: defaultName.projectName,
      when: answers => {
        return options.action === "create" || answers.action === "create";
      }
    });
  }

  questions.push({
    type: "confirm",
    name: "gitlab",
    message: "create project on gitlab",
    default: false,
    when: answers => {
      return options.action === "create" || answers.action === "create";
    }
  });

  questions.push({
    type: "confirm",
    name: "graphql",
    message: "Install a local database server",
    default: false,
    when: answers => {
      return options.action === "create" || answers.action === "create";
    }
  });

  questions.push({
    type: "input",
    name: "graphqlName",
    message: "database server name",
    default: "graphql",
    when: answers => {
      return (
        (options.action === "create" || answers.action === "create") &&
        answers.graphql
      );
    }
  });

  const answers = await inquirer.prompt(questions);

  if (options.action === "create" || answers.action === "create") {
    return {
      ...options,
      action: options.action || answers.action,
      projectName: options.projectName || answers.projectName,
      graphql: answers.graphql,
      graphqlName: answers.graphqlName,
      createGitlabRepo: answers.gitlab
    };
  } else {
    return {
      updateCore: true
    };
  }
}

export async function cli(args) {
  let options = parseArgumentsIntoOptions(args);

  options = await promptForMissingOptions(options);

  if (options.action === "create") {
    await createProject(options);
  } else {
    await coreUpdate();
  }
}
