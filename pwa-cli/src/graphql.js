import execa from "execa";

export async function cloneGraphQL(options) {
  const result = await execa("git", [
    "clone",
    "git@gitlab.com:unifygroup/aufeminin/mobile/aufql.git",
    options.graphqlName
  ]);

  if (result.failed) {
    return Promise.reject(new Error("Failed to clone repository"));
  }
  return;
}
