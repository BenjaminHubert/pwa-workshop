import Listr from "listr";
import chalk from "chalk";
import { projectInstall } from "pkg-install";
import { cloneRepo } from "./template";
import { cloneGraphQL } from "./graphql";
import { addCore, updateCore } from "./core";

export async function coreUpdate(options) {
  return await updateCore();
}

export async function createProject(options) {
  const tasks = new Listr([
    {
      title: "Cloning repository",
      task: () => cloneRepo(options)
    },
    {
      title: "Adding CORE subtree",
      task: () => addCore(options)
    },
    {
      title: "Installing PWA dependencies",
      task: () =>
        projectInstall({
          cwd: process.cwd() + "/" + options.projectName
        })
    },
    {
      title: "Cloning graphql repository",
      task: () => cloneGraphQL(options),
      skip: () => !options.graphql
    },
    {
      title: "Installing graphql dependencies",
      task: () =>
        projectInstall({
          cwd: process.cwd() + "/" + options.graphqlName
        }),
      skip: () => !options.graphql
    }
  ]);

  // const taskDev = new Listr([
  //   {
  //     title: "Cloning repository",
  //     task: () => cloneRepo(options)
  //   },
  //   {
  //     title: "Adding CORE subtree",
  //     task: () => addCore(options)
  //   }
  // ]);

  await tasks.run();
  console.log("%s Project ready", chalk.green.bold("DONE"));

  return true;
}
