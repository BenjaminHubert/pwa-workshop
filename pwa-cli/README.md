# Install

create a workspace directory

```bash
cd
mkdir pwa-workspace
```

go to your fresh workspace

```bash
cd pwa-workspace
```

clone this repo

```bash
git clone git@gitlab.com:unifygroup/aufeminin/dev/pwa-cli.git
```

install cli

```bash
cd pwa-cli
npm link
```

# Create new pwa

back to your workspace

```bash
cd ..
```

run pwa cli

```bash
pwa
```

# Start project

got to your database directory

```bash
cd my-database-folder
npm run dev
```

start a new terminal session
go to your pwa directory

```bash
cd my-pwa-folder
npm run devlocal
```

browse to http://localhost:3000
