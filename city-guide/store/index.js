import { init } from "./core.js";
import siteConfig from "../site.config";

export const state = () => {};

export const mutations = {};

export const getters = {};

export const actions = {
  async nuxtServerInit(storeContext, pageContext) {
    init(storeContext, pageContext, siteConfig);
  }
};
