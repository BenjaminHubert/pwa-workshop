export const state = () => {
  return {
    countries: []
  };
};

export const mutations = {
  cache(state, data) {
    if (data && !state.countries[data.name]) {
      state.countries[data.name] = data;
    }
  }
};

export const getters = {
  get: state => name => {
    return state.countries[name];
  }
};
