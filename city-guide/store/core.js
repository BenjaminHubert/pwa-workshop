import {
  coreState,
  coreMutations,
  coreGetters,
  initCore
} from "../core/store/index.js";

export const state = coreState;

export const mutations = coreMutations;

export const getters = coreGetters;

export const init = async (storeContext, pageContext, siteConfig) => {
  return initCore(storeContext, pageContext, siteConfig);
};
