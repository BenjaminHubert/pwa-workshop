export const state = () => {
  return {
    cities: []
  };
};

export const mutations = {
  cache(state, data) {
    if (!state.cities[data.city]) {
      state.cities[data.city] = data;
    }
  }
};

export const getters = {
  get: state => name => {
    return state.cities[name];
  }
};
