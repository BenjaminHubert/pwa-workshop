// constants.js
const CORE = "~/components/../core";

export const PATH = {
  CORE: {
    COMPONENTS: `${CORE}/components`,
    PLUGINS: `${CORE}/plugins`
  }
};
