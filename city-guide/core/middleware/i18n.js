export default async function({ isHMR, app, store, req }) {
  //   // If middleware is called from hot module replacement, ignore it
  if (isHMR) {
    return;
  }

  const locales = [
    {
      code: "it",
      domain: "www.alfemminile.com"
    },
    {
      code: "es",
      domain: "www.enfemenino.com"
    },
    {
      code: "fr",
      domain: "www.aufeminin.com"
    },
    {
      code: "fr",
      domain: "www.marmiton.org"
    }
  ];

  if (store.state.core.site) {
    // //setting locale in store based on domain
    if (!store.state.core.locale) {
      let domain = req.headers.host.split(":")[0].replace("localpwa", "www");
      let localCode = locales.filter(locale => locale.domain === domain)[0]
        .code;

      store.commit("core/setLocale", localCode);
      app.i18n.locale = localCode;
    }

    //setting messages
    if (!store.state.core.messages) {
      const messages = await fetch(store.state.core.graphQLEndPoint, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json"
        },
        body: JSON.stringify({
          query: `{
        allTranslations(siteId: ${store.state.core.site.Aff_SiteID}) {
            Sentence
            Context
            Translation
        }
      }`
        })
      });

      let messagesData = await messages.json();

      function groupMessagesByContext(memo, item) {
        var context = memo[item.Context];

        if (!context) {
          memo[item.Context] = {};
        }

        memo[item.Context][item.Sentence] = item.Translation;

        return memo;
      }

      let messagesGrouped = messagesData.data.allTranslations.reduce(
        groupMessagesByContext,
        {}
      );

      store.commit("core/setMessages", messagesGrouped);
      app.i18n.setLocaleMessage("fr", messagesGrouped);
    }
  }
}
