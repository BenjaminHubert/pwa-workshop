export const coreState = () => ({
  concatIsLoaded: false,
  logs: [],
  embeds: [],
  site: null,
  globalConf: null,
  bannerAtfNeedResize: true,
  modaleOpen: false,
  locale: null,
  messages: null
});

export const coreMutations = {
  disableScrollToSwipe(state) {
    state.scrollToSwipe = false;
  },

  enableScrollToSwipe(state) {
    state.scrollToSwipe = true;
  },
  askBannerAtfResize(state) {
    state.bannerAtfNeedResize = true;
  },

  markBannerAtfAsResized(state) {
    state.bannerAtfNeedResize = false;
  },

  markConcatAsLoaded(state) {
    state.concatIsLoaded = true;
  },

  addEmbed(state, src) {
    if (state.embeds.indexOf(src) === -1) {
      state.embeds.push(src);
    }
  },

  initSiteConfig(state, data) {
    state.site = data;
  },

  initGlobalConfig(state, conf) {
    state.globalConf = conf;
  },

  initSite(state, site) {
    state.site = site;
  },

  initDomain(state, domain) {
    state.domain = domain;
  },

  setSite(state, site) {
    state.site = site;
  },

  setModaleOpen(state) {
    state.modaleOpen = true;
  },
  setModaleOpen(state) {
    state.modaleOpen = true;
  },

  setLocale(state, locale) {
    state.locale = locale;
  },

  setModaleClose(state) {
    state.modaleOpen = false;
  },

  setGraphQLEndpoint(state, endpoint) {
    state.graphQLEndPoint = endpoint;
  },

  setMessages(state, messages) {
    state.messages = messages;
  },

  addMessage(state, payload) {
    console.log("store add message", payload.context, payload.sentence);
    state.messages[payload.context][payload.sentence] = payload.sentence;
  }
};

export const coreGetters = {
  isModalOpen: state => () => {
    return state.modaleOpen;
  }
};

const isReachable = require("is-reachable");

export const initCore = async (storeContext, pageContext, siteConfig) => {
  return initCoreStore(storeContext, pageContext, siteConfig);
};

const initCoreStore = async ({ commit }, { req }, siteConfig) => {
  const graphQLisAlive = await isReachable(siteConfig.graphQLEndPoint);

  if (graphQLisAlive) {
    let domain = req.headers.host.split(":")[0].replace("localpwa", "www");
    commit("core/initDomain", domain);
    commit("core/initGlobalConfig", siteConfig.globalConf);
    commit("core/setGraphQLEndpoint", siteConfig.graphQLEndPoint);

    let siteData;
    const ip = require("ip");
    //init site
    console.log("domain", domain);
    if (
      domain !== "localhost" &&
      domain !== ip.address() &&
      !domain.includes("ngrok")
    ) {
      const site = await fetch(siteConfig.graphQLEndPoint, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json"
        },
        body: JSON.stringify({
          query: `{
            site(host: "${domain}") {
              Site
              Aff_SiteID
              CodeLang
            }
          }`
        })
      });

      siteData = await site.json();
    } else {
      siteData = {
        data: {
          site: {
            Aff_SiteID: 1,
            Site: "barcelone",
            CodeLang: "fr"
          }
        }
      };
    }

    commit("core/setLocale", siteData.data.site.CodeLang);
    commit("core/setSite", siteData.data.site);
  }
};
