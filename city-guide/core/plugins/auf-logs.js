import Vue from "vue";

export default ({ store }) => {
  if (process.env.NODE_ENV === "production") {
    store.state.logs = [];

    if (process.browser) {
      window.aflogQueue = window.aflogQueue || [];
      window.onNuxtReady(() => {
        store.state.logs.forEach(log => {
          var func = function() {
            afLogger.logError(log, "PWA");
          };
          window.aflogQueue.push(func);
        });
      });
    }

    Vue.config.errorHandler = function(err) {
      var func = function() {
        afLogger.logError(err.message, "PWA");
      };

      if (process.browser) {
        window.aflogQueue = window.aflogQueue || [];
        window.aflogQueue.push(func);
      } else {
        store.state.logs.push(err.message);
      }
    };
  }
};
