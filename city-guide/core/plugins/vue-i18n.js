import Vue from "vue";
import VueI18n from "vue-i18n";

const vsprintf = require("sprintf-js").vsprintf;

class CustomFormatter {
  constructor() {}

  interpolate(message, values) {
    let resolvedString = vsprintf(message, values);
    return [resolvedString];
  }
}

Vue.use(VueI18n);

export default async ({ app, store }) => {
  // Set i18n instance on app
  // This way we can use it in middleware and pages asyncData/fetch

  if (!app.i18n) {
    app.i18n = new VueI18n({
      locale: store.state.core.locale,
      fallbackLocale: "fr",
      formatter: new CustomFormatter(),
      missing: (locale, key) => {
        let missing = {
          lang: locale,
          text: key
        };

        console.log("missing translation", missing);

        let context = key.includes(".") ? key.split(".")[0] : "Error";
        let sentence = key.includes(".") ? key.split(".")[1] : key;

        if (context === "Error") {
          return console.error(
            `missing context for sentence "${key}", please provide one.`
          );
        }

        fetch(store.state.core.graphQLEndPoint, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json"
          },
          body: JSON.stringify({
            query: `mutation {
              setTranslation(context:"${context}", siteId:${store.state.core.site.Aff_SiteID}, sentence:"${sentence}")
            }`
          })
        });

        store.commit("core/addMessage", {
          context: context,
          sentence: sentence
        });

        app.i18n.setLocaleMessage("fr", store.state.core.messages);
      }
    });
  }
  app.i18n.setLocaleMessage("fr", store.state.core.messages);
};
