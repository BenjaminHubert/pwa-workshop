import Vue from 'vue'
import VueLazyload from 'vue-lazyload'

Vue.use(VueLazyload, {
  lazyComponent: true,
  observer: true,
  observerOptions: {
    rootMargin: '400px 0px 400px 0px',
    threshold: 0
  }
})
