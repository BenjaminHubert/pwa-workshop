import VueSticky from 'vue-sticky' // Es6 module
import Vue from 'vue'

if(process.browser) {
    Vue.directive('sticky', VueSticky)
}