import Vue from "vue";
import PageNotFound from "~/core/components/PageNotFound.vue";

Vue.component("page-not-found", PageNotFound);
