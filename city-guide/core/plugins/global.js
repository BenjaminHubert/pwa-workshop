import { PATH } from "../constants";

export default ({ app }) => {
  app.PATH = PATH;
};
