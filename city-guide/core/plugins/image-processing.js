import Vue from 'vue';

Vue.prototype.$imageCropInfo = function(url) {
    //TODO: Optimise all of this ASAP (07/11/2018)
    //Update regex
    let lastURLComp = url.substr(url.lastIndexOf('/') + 1);
    let rawCropInfo = lastURLComp.split(".")[0].split("_")[1];
    let allValues = rawCropInfo.match(/[a-z]+([0-9]+)/g)

    let keysAndValues = {};
    allValues.forEach((item) => {
        keysAndValues[item.match(/([a-z]+)/g)] = Number(item.match(/([0-9]+)/g));
    });
    console.log(keysAndValues);

    this.cropX = keysAndValues.cx;
    this.cropY = keysAndValues.cy;
    this.width = keysAndValues.w;
    this.height = keysAndValues.h;

    console.log(this.cropX, this.cropY, this.width, this.height)
}