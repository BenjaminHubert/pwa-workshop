# PWA

## Installation

### Node.js

Make sure you have Node.js installed on your machine using this command : 

```bash
node -v
```

<aside class="notice">
Version number should be 10.15+.
</aside>

If you don't, install or update Node.js using n [https://nodejs.org/en/](https://nodejs.org/en/):

```bash
sudo npm install -g n
sudo n stable
````

### GIT

[https://git-scm.com/downloads](https://git-scm.com/downloads)

### Cloning repository

To start your experience with PWA, grab all sources files of the project on your computer.

via ssh

```bash
$ git clone git@gitlab.com:unifygroup/aufeminin/mobile/pwa-nuxt.git
```

or via https

```bash
$ git clone https://gitlab.com/unifygroup/aufeminin/mobile/pwa-nuxt.git
```

> Just copy one of this command in your terminal prompt, then press Enter

## Setting up

### Install dependencies

Go to the root directory of the project on your computer

```bash
$ cd pwa-nuxt
```

Then install dependencies

```$bash
$ npm i
```

> We use npm which is distributed with Node.js. This means that when you download Node.js, npm is automatically installed on your computer. To confirm that you have npm install, you can run this command in your terminal

```bash
$ npm -v
```

> If npm is not on your computer, please download and install here:
> [https://www.npmjs.com/get-npm](https://www.npmjs.com/get-npm)

## Development Workflow

### Make a new local branch

> You can't directly work on master branch because you can't push your changes directly on master branch, no you can't ! So to start coding a new feature or fixing a bug you will have to make a local branch, then push that branch on the remote repository and request for merge.

First,  create a local branch on your computer

```bash
$ git checkout -b my-awesome-new-branch
```

> Please prefix new features with feature-my-awesome-new-feature and bug fix with fix-my-hot-fix

### Develop

Start coding your new feature or fix a bug, then test on server. 

### Test on server

The common way to start coding on PWA is to run a local server with hot reload, i.e the server will inject the new code into the page whenever you modify a file.

There are two way for starting the dev server :

- the mixed mode: pwa is local and graphQL server is remote

- the full local mode: pwa and graphQL are both local

#### Mixed mode (local pwa and remote graphQL)

Start you server with hot reload by using the following command

```bash
$ npm run dev
```

> This will start pwa server at [http://localhost:3000](http://localhost:3000) and graphql server at [https://graphql.aufeminin.com](https://graphql.aufeminin.com) (only available @ HQ office)

#### Full local mode (local pwa and local graphQL)

It is common to use a graphql local server to test features, you can do that with the **devlocal** command:

```bash
$ npm run devlocal
```

> This will start pwa server at [http://localhost:3000](http://localhost:3000)

Then start the graphql server by following instructions in the aufql repository readme.

##### TL;DR

under the root of your aufql local repository, run the following command

```bash
$ npm run dev
```

### Building

The build part is fully automated by the Gitlab CI/CD process but you may have to test a built version localy (for instance, to test service worker)

#### Build for production

```bash
$ npm run build
```

##### Start built version

```bash
$ npm run start
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).

### Push work to remote repo

After coding your awesome stuff, commit you work and push your branch to remote repository to then be able to merge into master branch

```bash
$ git push --set-upstream origin my-awesome-new-branch
```

#### Merge request

Once your branch is pushed to remote repository, you'll be able to make a merge request on master branch and the deploy you work. Git will show you the direct link to the merge request form, in this exemple: 

https://gitlab.com/unifygroup/aufeminin/mobile/pwa-nuxt/merge_requests/new?merge_request%5Bsource_branch%5D=lock-swipe-on-modale

```bash
$ git push --set-upstream origin lock-swipe-on-modale
Counting objects: 6, done.
Delta compression using up to 8 threads.
Compressing objects: 100% (6/6), done.
Writing objects: 100% (6/6), 788 bytes | 788.00 KiB/s, done.
Total 6 (delta 5), reused 0 (delta 0)
remote: 
remote: To create a merge request for lock-swipe-on-modale, visit:
remote:   **https://gitlab.com/unifygroup/aufeminin/mobile/pwa-nuxt/merge_requests/new?merge_request%5Bsource_branch%5D=lock-swipe-on-modale**
remote: 
To gitlab.com:unifygroup/aufeminin/mobile/pwa-nuxt.git
 * [new branch]      lock-swipe-on-modale -> lock-swipe-on-modale
Branch 'lock-swipe-on-modale' set up to track remote branch 'lock-swipe-on-modale' from 'origin'.
```

You can "cmd + click" on the merge request url provided.

This will open the merge request form on your favorite web browser. 

On this page

- Edit the request title if needed
  
  ![](docs/merge-request-form-title.png)
- Check source branch. Should be "your-awesome-new-branch"
- Check destination branch. Should be "master"
  
  ![](docs/merge-request-form-branches.png)
- Check the box "Delete source branch when merge request is accepted" if you want your branch to be deleted from the remote repository after merge
  
  ![](docs/merge-request-form-delete.png)
- Click "Submit merge request" button
  
  ![](docs/merge-request-form-button.png)



On the next page

- Wait for pipeline to finish running

![](docs/merge-wait-pipeline.png)

> During this step, if your are ok with your merge, you can click on the "Merge when pipeline succeeds" button. This will automatically merge your-awesome-new-feature branch to master after pipeline completion.
> 
> Otherwise, wait for pipeline completion and then click "merge" button.

![](docs/merge-merge-button.png)



### Deploy

As explained above, the production branch is the master one. Now that you complete the merge process, you can deploy your changes to make them available to users, using **pipelines**



On the left bar menu, click on CI/CD then select **Pipelines**. This will show you the list of available merges to deploy.

![](docs/deploy-start-pipeline.png)

Click on the "play" button located on the right of the list, the one that correspond to your merge and select "deploy:production".

This will run the Pipeline that will deploy all your changes on the server.

> You can follow all these steps on the dedicated PWA slack channel
> 
> https://aufeminin.slack.com/messages/CEZSWDS5Q

### CONGRATS! YOU'ARE DONE!



## Troubleshooting

### App crash on startup/build not passed

it is usualy a node modules issue just remove them and re-install

```bash
$ rm -rf node_modules
$ npm i
```
