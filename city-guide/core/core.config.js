import siteConfig from "../site.config";

export default {
  css: ["~/core/assets/ads.css"],
  configureWebpack: {
    devtool: "source-map",
    configureWebpack: { output: { globalObject: "this" } }
  },
  plugins: [
    "~/core/plugins/global.js",
    "~/core/plugins/vue-lazyload",
    "~/core/plugins/vue-hammer",
    "~/core/plugins/vue-auf-image",
    "~/core/plugins/image-processing",
    "~/core/plugins/vue-not-found",
    "~/core/plugins/auf-logs",
    "~/core/plugins/vue-i18n",
    "~/core/plugins/vue-babel-polyfill",
    { src: "~/core/plugins/vue-sticky", ssr: false },
    { src: "~/core/plugins/intersection-observer-polyfill", ssr: false },
    { src: "~/core/plugins/vue-video-js" }
  ],
  modules: [
    ["@nuxtjs/pwa", { icon: false }],
    "nuxt-custom-headers",
    [
      "@nuxtjs/google-analytics",
      {
        id: "UA-1901803-14",
        debug: {
          enabled: false
        }
      }
    ]
  ],
  apollo: {
    tokenName: "yourApolloTokenName", // optional, default: apollo-token
    includeNodeModules: true, // optional, default: false (this includes graphql-tag for node_modules folder)
    authenticationType: "Basic", // optional, default: 'Bearer'
    // required
    clientConfigs: {
      default: {
        httpEndpoint: siteConfig.graphQLEndPoint,

        // optional
        // See https://www.apollographql.com/docs/link/links/http.html#options
        /* httpLinkOptions: {
          credentials: 'same-origin'
        },*/
        // You can use `wss` for secure connection (recommended in production)
        // Use `null` to disable subscriptions
        // wsEndpoint: 'http://localhost:4000', // optional
        // LocalStorage token
        tokenName: "apollo-token", // optional

        // Enable Automatic Query persisting with Apollo Engine
        persisting: false, // Optional
        // Use websockets for everything (no HTTP)
        // You need to pass a `wsEndpoint` for this to work
        websocketsOnly: false // Optional
      }
    }
  },
  /*
   ** Customize the progress bar color
   */
  loading: { color: "#3B8070" },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** Run ESLint on save
     */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/
        });
      }

      if (isClient) {
        config.node = {
          fs: "empty",
          child_process: "empty"
        };
      }
    }
  }
};
