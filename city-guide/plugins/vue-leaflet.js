import Vue from "vue";
import L from "leaflet";
import "leaflet/dist/leaflet.css";

const LeafletPlugin = {
  install(Vue) {
    // Expose Leaflet
    Vue.prototype.$L = L;
  }
};

Vue.use(LeafletPlugin);
