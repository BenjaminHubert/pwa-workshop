/*eslint no-unused-vars: ["error", { "args": "none" }]*/
import coreConfig from "./core/core.config.js";
import { deepmerge } from "./core/deepmerge.js";

const conf = {
  css: [
    "assets/main.scss",
    "assets/page-transitions.scss",
    "assets/dark.scss",
    "assets/barcelone/main.scss"
  ],
  router: {
    middleware: "importCoreRouterMiddleware"
  },
  layoutTransition: {
    name: "page"
  },
  plugins: [
    { src: "~/plugins/demo/leaf", ssr: false },
    { src: "~/plugins/vue-leaflet", ssr: false }
  ],
  modules: [
    "@nuxtjs/apollo",
    "nuxt-custom-headers",
    // Doc: https://bootstrap-vue.js.org/docs/
    "bootstrap-vue/nuxt",
    "@nuxtjs/axios"
  ],
  // i18n: locales,
  manifest: {
    name: "Aufeminin",
    lang: "fa",
    theme_color: "#951f60"
  },
  /*
   ** Headers of the page
   */
  head: {
    title: "pwa-nuxt",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: "poc pwa vuejs avec rendu server side"
      }
    ],
    link: [
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/css?family=Monoton&display=swap"
      },
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" }
    ],
    __dangerouslyDisableSanitizers: ["script"]
  },
  /*
   ** Customize the progress bar color
   */
  loading: { color: "#3B8070" }
};

module.exports = deepmerge(coreConfig, conf);
