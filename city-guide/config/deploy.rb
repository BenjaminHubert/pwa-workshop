# config valid for current version and patch releases of Capistrano
lock '~> 3.11.0'

set :application, 'pwa'
set :app_command, 'pm2.yml'
set :repo_url, 'git@gitlab.com:unifygroup/aufeminin/mobile/pwa-nuxt.git'

set :branch, ENV['CI_COMMIT_SHA'] if ENV['CI_COMMIT_SHA']

set :deploy_to, '/app/aufeminin/pwa'

set :npm_flags, '--silent --no-progress'

# Default value for :linked_files is []
append :linked_files, 'pm2.yml'

# Default value for linked_dirs is []
# append :linked_dirs, "node_modules"
set :copy_files, ['node_modules']

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Default value for keep_releases is 5
# set :keep_releases, 5

# Uncomment the following to require manually verifying the host key before first deploy.
# set :ssh_options, verify_host_key: :secure

namespace :pwa do
  desc 'Copy old JS'
  task :copy_old_js do
    on roles fetch(:npm_roles) do
      releases = capture(:ls, '-xtr', releases_path).split
      execute :rsync, "-Haurolv --numeric-ids #{releases_path}/#{releases[-2]}/.nuxt/dist/client/ #{releases_path}/#{releases[-1]}/.nuxt/dist/client/"
    end
  end

  desc 'Remove old JS'
  task :remove_old_js do
    on roles fetch(:npm_roles) do
      releases = capture(:ls, '-xtr', releases_path).split
      execute :find, "#{releases_path}/#{releases[-2]}/.nuxt/dist/client/ -type f -mtime +30 -name '*.js' -delete -print"
    end
  end
end

namespace :npm do
  desc 'Build app node'
  task :build do
    on roles fetch(:npm_roles) do
      within fetch(:npm_target_path, release_path) do
        with fetch(:npm_env_variables, {}) do
          execute :npm, 'run build', fetch(:npm_flags)
        end
      end
    end
  end

  before :install, :prune
  after :install, :build
end

namespace :deploy do
  desc 'Restart application'
  task :restart do
    invoke 'pm2:restart'
  end

  before :publishing, 'pwa:copy_old_js'
  after :publishing, :restart
  after :cleanup, 'pwa:remove_old_js'
end
