let endpoint = "https://graphql.aufeminin.com";

process.argv.forEach(function(val) {
  if (val === "--local-gql" || val === "--local-graphql") {
    endpoint = "http://localhost:4000/graphql";
  }
});

export default {
  graphQLEndPoint: endpoint,
  globalConf: {
    videos_base_domain: "https://assets.afcdn.com/video",
    images_base_domain: "https://assets.afcdn.com" //'https://image.afcdn.com'
  }
};
