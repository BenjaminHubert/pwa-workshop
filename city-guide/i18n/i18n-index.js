export default {
  locales: [
    {
      code: "it",
      domain: "localpwa.alfemminile.com:3000"
    },
    {
      code: "es",
      domain: "localpwa.enfemenino.com:3000"
    },
    {
      code: "fr",
      domain: "localpwa.aufeminin.com:3000"
    }
  ],
  differentDomains: true,
  vueI18n: {
    fallbackLocale: "fr",
    messages: {
      fr: {
        par: "par",
        welcome: "Bienvenue"
      },
      es: {
        par: "por",
        welcome: "Bienvenido"
      },
      it: {
        par: "di",
        welcome: "Bienvenido"
      }
    }
  }
};
