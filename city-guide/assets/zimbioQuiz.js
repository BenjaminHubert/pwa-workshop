/* globals FB */
// TODO: refactor
/*eslint no-unused-vars: off*/

(function(root, factory) {
	window.CrowdLeap = factory(window);

	/*if (!root.CrowdLeap.isInitialized()) {
		root.CrowdLeap.init();
	}*/
}(this, function(window) {
	var gameContainers,
	gameClass = 'crowdleap-game',
	origin = '//widget.crowdleap.com',
	events = [
	'crowdleap.load',
	'crowdleap.start',
	'crowdleap.setanswer',
	'crowdleap.pageview',
	'crowdleap.complete',
	'crowdleap.playagain',
	'crowdleap.share'
	],
	frames = {},
	frame,
	frameMaxWidth = 640,
	frameMinWidth = 250,
	frameMinHeight = 200,
	i,
	eventCallbacks = {},
	initialized = false,
	messagePrefix = '__CROWDLEAP__',
	userConfig = {
		auto_scroll: true,
		share: true,
		fb_native_app: navigator.userAgent.match(/(iPhone|iPad).*(FBAV)/i) ? true : false
	};

	/**
	* Event callbacks object will store the users
	* callback functions by event name. Callback functions
	* will be pushed onto the stack with
	* CrowdLeap.subscribeEvent('event', callback);
	*/
	for (i=0; i<events.length; i++) {
		eventCallbacks[events[i]] = [];
	}

	/**
	* Generate the embed url (the iframe src).
	*
	* @param string id - the game object guid
	* @param string type - the game type
	* @param object params - js obj to be turned into qs params
	*/
	function getEmbedURL(id, type, params) {
		var url,
		queryList = [];

		url = origin + '/crowdleap_embed/' + type + '/' + id;

		// Build the query string parameters
		if (params) {
			for (var key in params) {
				queryList.push(key + "=" + encodeURIComponent(params[key]));
			}
		}

		if (queryList.length) {
			url += "?" + queryList.join("&");
		}

		return url;
	}

	/**
	* Build the iframe based on the embed code.
	*
	* @param object container - the container DOM object
	*/
	function buildFrameFromContainer(container) {
		var id,
		idParts,
		height,
		width,
		tracking,
		result,
		type,
		shareUrl,
		frameConfig = {},
		urlParams = {};

		// Handle user data attributes
		id = container.getAttribute('data-id');

		// See if the data-id contains a type
		if (id.length > 11) {
			if (id.indexOf(":") !== -1) {
				idParts = id.split(":");
				type = idParts[0];
				id = idParts[1];
			} else {
				// Take the last 11 characters in an attempt
				// to make this right
				id = id.substr(-11);
			}
		}

		if (!type) type = 'quiz';

		width = parseInt(container.getAttribute('data-width'));
		if (!isNaN(width)) {
			width = Math.min(width, frameMaxWidth);
			width = Math.max(width, frameMinWidth);
			frameConfig.width = width; // Set as int
			width = width + 'px';
		} else {
			width = '100%';
		}

		height = container.getAttribute('data-height');
		if (height === "expand") {
			frameConfig.height = "expand";
		} else {
			height = parseInt(height);
			if (!isNaN(height)) {
				height = Math.max(height, frameMinHeight);
				frameConfig.height = height; // Set as int
				frameConfig.resize = false;
				height = height + 'px';
			}
		}

		color = container.getAttribute('data-color');
		if (color) {
			color = color.replace('#', '');
			if (/^[a-f0-9]{3,6}$/i.test(color)) {
				urlParams.data_color = color;
			}
		}

		tracking = container.getAttribute('data-tracking');
		if (tracking && tracking === "false") {
			urlParams.tracking = "false";
		}

		result = container.getAttribute('data-result');
		if (result) {
			urlParams.result = result;
		}

		// Create the frame
		frame = document.createElement('iframe');
		frame.style.borderWidth = '0px';
		frame.style.width = width;
		frame.style.overflowX = 'hidden';
		if (height && height !== "expand") {
			frame.style.height = height;
		} else {
			frame.style.overflowY = 'hidden';
		}
		frame.style.maxWidth = frameMaxWidth + 'px';
		frame.style.minWidth = frameMinWidth + 'px';
		frame.src = getEmbedURL(id, type, urlParams);

		frames[id] = {};
		frames[id].frame = frame;

		// Set the config
		shareUrl = container.getAttribute('data-share-url');
		if (shareUrl) {
			frameConfig.shareUrl = shareUrl;
		} else {
			frameConfig.shareUrl = window.location.href;
		}

		frames[id].config = frameConfig;

		return id;
	}

	/**
	* Resize a frame
	*
	* @param string frame_id - the game object guid
	* @param integer height - the desired height
	* @param boolean force - force resize
	*/
	function resizeFrame(frame_id, height, force) {
		var fConf = frames[frame_id].config,
		f = frames[frame_id].frame,
		currentHeight;

		force = force || false;

		// Is this frame allowed to resize? Default to true
		if (!force && typeof fConf.resize !== "undefined" && fConf.resize === false) return;

		if (!force && (typeof fConf.height !== "undefined") && fConf.height === "expand") {
			currentHeight = parseInt(f.style.height);

			if (height <= currentHeight) return;
		}

		f.style.height = height + 'px';
	}

	/**
	* Scroll to the top of the frame
	*
	* @param string frame_id - the game object guid
	*/
	function scrollTopFrame(frame_id, force) {
		if (userConfig.auto_scroll || force) {
			window.scrollTo(0, frames[frame_id].frame.offsetTop);
		}
	}

	/**
	* Fire the callbacks for a given event name
	*
	* @param string eventName  - the event name
	* @param object response - the response object
	*/
	function fireCallbacks(eventName, response) {
		var callbacks = eventCallbacks[eventName];

		response = (response || {});
		response.status = 'success';

		if (callbacks && callbacks.length) {

			for (var i=0; i<callbacks.length; i++) {
				callbacks[i](response);
			}
		}
	}

	/**
	* Handle an event
	*
	* @param obj message
	*
	* @param string frame_id - the game object guid
	* @param object ev - the event passed from the parent
	*/
	function handleEvent(frame_id, ev) {
		var cEvent,
		theFrame = frames[frame_id].frame;

		switch (ev.event) {
			case 'load':
			fireCallbacks('crowdleap.load', {
				frame_id: frame_id
			});
			break;
			case 'start':
			fireCallbacks('crowdleap.start', {
				frame_id: frame_id
			});
			break;
			case 'setanswer':
			fireCallbacks('crowdleap.setanswer', {
				frame_id: frame_id,
				question: ev.question,
				answer: ev.answer
			});
			break;
			case 'pageview':
			fireCallbacks('crowdleap.pageview', {
				frame_id: frame_id
			});
			break;
			case 'complete':
			fireCallbacks('crowdleap.complete', {
				frame_id: frame_id
			});
			break;
			case 'share':
			fireCallbacks('crowdleap.share', {
				frame_id: frame_id,
				type: ev.type,
				data: ev.data
			});
			break;
			case 'playagain':
			fireCallbacks('crowdleap.playagain', {
				frame_id: frame_id
			});
			break;
		}
	}

	/**
	* Retreat. Handle a broken or deleted game.
	*
	* This occurs when the child needs to bail out
	* for some reason.
	*/
	function handleRetreat(frame_id) {
		var frame = frames[frame_id].frame;
		frame.parentNode.removeChild(frame);
	}

	/**
	* Handle an individual message
	*
	* @param string frame_id (game guid)
	* @param object message
	*/
	function handleMessage(frame_id, message) {
		if (!message.name) return;
		message.values = message.values || {};

		switch (message.name) {
			case 'resize':
			resizeFrame(frame_id, message.values.height, message.values.force);
			break;
			case 'redirect':
			window.location = message.values.url;
			break;
			case 'scrollTop':
			scrollTopFrame(frame_id, message.values.force);
			break;
			case 'event':
			handleEvent(frame_id, message.values);
			break;
			case 'retreat':
			handleRetreat(frame_id);
			break;
		}
	}

	/**
	* Handle messages sent up from the frame.
	*
	* @param object e - event object
	*/
	function messageReceived(e) {
		var data;
		if (typeof e.data.indexOf === 'undefined' || e.data.indexOf(messagePrefix) !== 0) return;

		data = JSON.parse(e.data.substr(messagePrefix.length));

		if (!data.messages.length || !data.frame_id) {
			return;
		}

		for (var i=0; i<data.messages.length; i++) {
			handleMessage(data.frame_id, data.messages[i]);
		}
	}

	/**
	* Subscribe Event
	*
	* Give the user the abilty to subscribe to an event
	* @param string event name
	* @param function callback
	*/
	function subscribeEvent(eventName, callback) {
		if (events.indexOf(eventName) === -1) {
			console.warn(eventName + ' is not a valid event.');
			return;
		}

		eventCallbacks[eventName].push(callback);
	}

	/**
	* Send a mesage to the child frame
	*
	* @param string frame_id (guid)
	* @param object message
	*/
	function messageChild(frame_id, message) {
		var frameWindow = frames[frame_id].frame.contentWindow || frames[frame_id].frame;
		frameWindow.postMessage(messagePrefix + JSON.stringify(message), '*');
	}

	/**
	* Send a userConfig to a child frame.
	*
	* @param string frame_id - the game object guid
	* @param object config - configuration to send
	*/
	function sendConfigToChild(frame_id, config) {
		messageChild(frame_id, {
			name: 'config',
			values: config
		});
	}

	/**
	* Initialize this module.
	*
	* @param object conf - user configuration for this module
	*    and the game to be loaded.
	*/
	function init(conf, containerId) {
		var attr,
		conf = conf || {};

		for (attr in conf) {
			userConfig[attr] = conf[attr];
		}

		// Send config to child frame
		eventCallbacks['crowdleap.load'].push(function(response) {
			sendConfigToChild(response.frame_id, userConfig);
		});

		initialized = true;

		createQuiz(containerId);
	}

	function isInitialized() {
		return initialized;
	}

	function createQuiz(containerId) {
		gameContainers = []
		if (containerId) {
			gameContainers.push(document.getElementById(containerId));
		} else {
			gameContainers = document.getElementsByClassName(gameClass);
		}

		if (!gameContainers.length) {
			return;
		}

		for (i = 0; i < gameContainers.length; i++) {
			var frameId = buildFrameFromContainer(gameContainers[i]);
			gameContainers[i].appendChild(frames[frameId].frame);
		}
	}

	window.addEventListener('message', messageReceived, false);

	/**
	* Add our own event listeners
	*/
	// We will always want to send user config to the iframe once it has loaded
	eventCallbacks['crowdleap.load'].push(function(response) {
		sendConfigToChild(response.frame_id, frames[response.frame_id].config);
	});

	subscribeEvent('crowdleap.share', function(data) {
		// Share facebook using FB.ui in the parent frame if we are inside of the native app
		if (data.type === 'facebook' && userConfig.share && userConfig.fb_native_app) {
			if (typeof FB !== 'undefined') {
				FB.ui(data.data);
			}
		}
	});

	return {
		init: init,
		subscribeEvent: subscribeEvent,
		isInitialized: isInitialized
	};
}));
