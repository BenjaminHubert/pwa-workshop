const StoryUtils = {
  ids: ['642124', '2925147', '637666', '2884112', '1543107', '2884100', '2884021', '2725276', '1100661', '2289674'],

  getNextId(currentStoryId) {
    let pos = this.ids.indexOf(currentStoryId);

    if(pos !== -1) {
      pos = pos === this.ids.length - 1 ? 0 : pos + 1
    }

    let storyId = this.ids[pos]

    return storyId
  },

  getPrevId(currentStoryId) {
    let pos = this.ids.indexOf(currentStoryId);

    if(pos !== -1) {
      pos = pos === 0 ? this.ids.length - 1 : pos - 1
    }
    let storyId = this.ids[pos]

    return storyId
  }
}
module.exports = StoryUtils
