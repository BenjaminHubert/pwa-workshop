const throttle = require("lodash.throttle");

let myHand = null;

export class NextStory {
  constructor(params) {
    this.set(params);
    this.store = params.store;
    this.handleScroll();
  }
  getPosition(element) {
    var xPosition = 0;
    var yPosition = 0;

    while (element) {
      xPosition += element.offsetLeft - element.scrollLeft + element.clientLeft;
      yPosition += element.offsetTop - element.scrollTop + element.clientTop;
      element = element.offsetParent;
    }

    return { x: xPosition, y: yPosition };
  }

  handleScroll() {
    let self = this;
    myHand = throttle(self.showNext.bind(self), 10);
    window.addEventListener("scroll", myHand, { passive: true });
  }

  unbind() {
    window.removeEventListener("scroll", myHand, { passive: true });
  }

  set(params) {
    this.container = params.container;

    this.onChange = params.onChange || this.onChange || null;
  }

  showNext() {
    if (!this.store.state.scrollToSwipe) {
      return;
    }

    let stories = document.querySelectorAll(".story-container");

    if (stories.length < 2) {
      return;
    }

    let content = stories[stories.length - 2].querySelector("article"),
      next = stories[stories.length - 1],
      contentRect = content.getBoundingClientRect(),
      scrollTop =
        window.pageYOffset ||
        (document.documentElement || document.body.parentNode || document.body)
          .scrollTop,
      seeAlsoHeight = document
        .querySelector(".see-also")
        .getBoundingClientRect().height,
      yend = scrollTop + window.innerHeight + seeAlsoHeight,
      y = yend - (contentRect.height + 600 + content.offsetTop),
      self = this;

    next.classList.add("next-story");

    if (y > 0) {
      let changeHandler = el => {
        el.classList.remove("moving");
        el.classList.add("show-next");
        el.style.transform = `translate3d(${0}%, 0, 0)`;

        //prevent display bug on modal
        setTimeout(function() {
          el.style.transform = "";
        }, 400);
      };

      var ratio = 100 / (window.innerHeight / y);
      if (ratio > 70) {
        changeHandler(next);

        if (!this.isTop) {
          setTimeout(function() {
            if (typeof self.onChange === "function") {
              self.onChange();

              //repeat changeHanlder to cover edge cases
              changeHandler(next);
            }
          }, 200);
          this.isTop = true;
        }
      } else {
        next.classList.add("moving");
        next.style.transform = `translate3d(${100 - ratio}%, 0 ,0)`;
        next.classList.remove("show-next");
        this.isTop = false;
      }
    } else {
      this.isTop = false;
      next.style.transform = `translate3d(100%, 0, 0)`;
    }
  }
}
