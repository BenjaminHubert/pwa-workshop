var AufVideo = AufVideo || {};
AufVideo.Publica = (function() {
	var adPlayer = function(opts) {
		this.opts = opts;
	};

	adPlayer.prototype = {
		constructor: adPlayer,
		init: function() {
			if (typeof(afAdblock) != 'undefined' && afAdblock.status == 0) {
				// No adblock: no need to initialize Publica script
				return;
			}
			
			if (typeof(window.pAPI) != 'function' || !this.opts) {
				this.publish('video.ad.setupError', {
					message: 'no publica player'
				});
				return;
			}

			this.adblockDetected = false;
			this.instance = window.pAPI;
			this.instance('init', {
				iu: this.opts.iu,
				sz: this.opts.sz,
				cust_params: this.opts.cust_params,
				custom1: this.opts.iu
			});
			this.videoBreak = 'preroll'; // Currently Publica is only preroll

			this.initListeners();
		},
		initListeners: function() {
			var that = this;

			// Event => Publica script has been loaded correctly
			this.instance.ready(function (data) {
				that.adblockDetected = data.adblock_detected;
				that.publish('video.ad.player.ready');
			});

			// Event => end of recovery process
			/* this.instance('event_end', function () { }); */

			// Event => ad available
			/* this.instance('event_filled', function () { }); */

			// Event => ad call returned a flash
			this.instance('event_unplayable', function () {
				that.publish('video.ad.notAvailable');
			});

			// Event => ad first frame displayed
			this.instance('event_impression', function () {
				that.publish('video.ad.start');
			});

			// Event => ad displayed during 2 seconds(50% inview)
			this.instance('event_viewable', function () {
				that.publish('video.ad.validated');
			});

			// Event => the user click on the ad
			this.instance('event_click', function () {
				that.publish('video.ad.click');
			});
		},
		publish: function(event, data) {
			data = data || {};
			data.adblockDetected = this.adblockDetected;
			data.adPlayerName = 'Publica'; // To override current adPlayer name
			data.playerId = this.opts.playerId; // We always need the playerId to retrieve it(Logs)
			data.videoBreak = this.videoBreak;
			PubSub.publish(event, data);
		}
	};

	return adPlayer;
})();
