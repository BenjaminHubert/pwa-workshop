// Official documentation: https://github.com/googleads/videojs-ima

var AufVideo = AufVideo || {};
AufVideo.IMA = (function() {
	// Private/Constants
	var IMA_UNFILLED_ERROR_CODES = {
		1009: 'Empty VAST response.'
	};

	var adPlayer = function(opts) {
		this.opts = opts || {};
	};

	adPlayer.prototype = {
		constructor: adPlayer,
		// Example integration: https://github.com/googleads/videojs-ima/blob/master/examples/autoplay/ads.js
		// IMPORTANT: this ad player needs to be initialized after player (video-js) synchronously
		onPlayerInit: function(player) {
			if (!window.google || !player || !player.playerInstance || typeof player.playerInstance.ima !== 'function') {
				this.publish('video.ad.setupError', { message: 'no ima player' });
				return;
			}
			if (!this.opts.adTagUrl) {
				this.publish('video.ad.setupError', { message: 'no ima tag url' });
				return;
			}

			var that = this;
			var options = {
				id: this.opts.id,
				adLabel: this.opts.adLabel,
				adWillAutoPlay: this.opts.adWillAutoPlay,
				adWillPlayMuted: this.opts.adWillPlayMuted,
				adsRenderingSettings: {
					enablePreloading: true,
					loadVideoTimeout: this.opts.loadVideoTimeout || 10000
				},
				disableCustomPlaybackForIOS10Plus: true, // IMPORTANT: IOS inline autoplay issues without this
				prerollTimeout: 5000,
				showCountdown: this.opts.showCountdown, // Whether or not to show the ad countdown timer
				vastLoadTimeout: 15000, // As we do prefetch we can try a higher value here

				adsManagerLoadedCallback: function() {
					// IMPORTANT: events need ads manager to be loaded
					that.initIMAListeners();

					// Override mute method by the IMA one(manager needs to be loaded)
					player.mute = that.mute;
				}
			};

			if (this.opts.language) {
				options.locale = this.opts.language.toLowerCase();
			}
			if (this.opts.numRedirects) {
				options.numRedirects = this.opts.numRedirects;
			}

			this.player = player;
			this.playerInstance = player.playerInstance;

			// Initialize IMA SDK
			this.playerInstance.ima(options);

			// Player video is in autoplay or startOnSight mode
			if (player.isAutoplay()) {
				if (!this.opts.noPrefetch) {
					// Manually prefetch preroll when player is ready
					this.playerInstance.one('ready', function() {
						this.requestAds();
					}.bind(this));
				}
			} else {
				// Initialize the ad container when the video player is clicked, but only the
				// first time it's clicked.
				var startEvent = 'click';
				if (navigator.userAgent.match(/iPhone/i) || 
						navigator.userAgent.match(/iPad/i) || 
						navigator.userAgent.match(/Android/i)) {
					startEvent = 'touchend';
				}

				this.playerInstance.one(startEvent, function() {
					// IMPORTANT: must be done via a user action if autoplay is not allowed
					this.playerInstance.ima.initializeAdDisplayContainer(); // Create an ad display container
					this.requestAds();
				}.bind(this));
			}

			// Errors listeners
			this.initErrorsListeners();
		},
		// TODO: use videojs-contrib-ads for both errors/events => IMA lib needs to match all events
		// Errors: https://github.com/videojs/videojs-contrib-ads
		initErrorsListeners: function() {
			var that = this;
			this.playerInstance.on('adserror', function(error) {
				if (IMA_UNFILLED_ERROR_CODES[error.data.AdError.getErrorCode()]) {
					that.publish('video.ad.unfilled');
				} else {
					that.publish('video.ad.error', {
						message: '[' + error.data.AdError.getErrorCode() + '] ' + error.data.AdError.getMessage()
					});
				}
			});
		},
		// Events: https://developers.google.com/interactive-media-ads/docs/sdks/html5/v3/apis#ima.AdEvent.Type
		initIMAListeners: function() {
			var that = this;

			this.playerInstance.ima.addEventListener(google.ima.AdEvent.Type.LOADED, function(data) {
				that.publish('video.ad.ready', { videoBreak: that.getState(data) });
			});
			this.playerInstance.ima.addEventListener(google.ima.AdEvent.Type.STARTED, function(data) {
				that.publish('video.ad.start', {
					file: that.player.getFile(), // Needed by GA to count unique Video Plays
					videoBreak: that.getState(data) 
				});
			});
			this.playerInstance.ima.addEventListener(google.ima.AdEvent.Type.COMPLETE, function(data) {
				that.publish('video.ad.end', { videoBreak: that.getState(data) });
			});
			this.playerInstance.ima.addEventListener(google.ima.AdEvent.Type.CLICK, function(data) {
				that.publish('video.ad.click', { videoBreak: that.getState(data) });
			});
			this.playerInstance.ima.addEventListener(google.ima.AdEvent.Type.IMPRESSION, function(data) {
				that.publish('video.ad.impression', { videoBreak: that.getState(data) });
			});

			// Allow to detect that content resume well after ads end
			this.playerInstance.ima.addEventListener(google.ima.AdEvent.Type.CONTENT_RESUME_REQUESTED, function() {
				that.publish('video.ad.contentresume');
			});
		},

		requestAdsDone: false,
		requestAds: function() {
			// Only request ads once by player instance
			if (this.requestAdsDone) {
				return;
			}
			this.requestAdsDone = true;
			this.playerInstance.ima.setContentWithAdTag(null, this.getUpdatedAdTagUrl(), false);
			this.playerInstance.ima.requestAds();
		},
		// We need to update some key/values on the fly for some 3rd party
		getUpdatedAdTagUrl: function() {
			// Allow to pass player height and width as key/values on the fly (player loaded)
			var custParamsKey = 'cust_params=';
			var custParamsIndex = this.opts.adTagUrl.indexOf(custParamsKey);

			if (custParamsIndex !== -1) {
				var custParamsKeyPos = custParamsIndex + custParamsKey.length;
				this.opts.adTagUrl = this.opts.adTagUrl.substr(0, custParamsKeyPos) 
							+ encodeURIComponent('playerheight=' + (this.player.getHeight() || 480) + '&') 
							+ encodeURIComponent('playerwidth=' + (this.player.getWidth() || 640) + '&') 
							+ this.opts.adTagUrl.substr(custParamsKeyPos);
			}

			return this.opts.adTagUrl;
		},
		// Doc => https://developers.google.com/interactive-media-ads/docs/sdks/html5/v3/apis#ima.AdPodInfo.getPodIndex
		getState: function(data) {
			switch(data.getAd().getAdPodInfo().getPodIndex()) {
				case 0:
					return 'preroll';
				case -1:
					return 'postroll';
				default:
					return 'midroll';
			}
		},
		mute: function() {
			// TODO: replace by better method
			// current best method is to simulate a mute click when player is not already muted
			if (!this.playerInstance.ima.controller.sdkImpl.isAdMuted()) {
				this.playerInstance.ima.controller.adUi.muteDiv.click();
			}

			// Prevent a bug where adMuted is not updated
			if (!this.playerInstance.muted()) {
				this.playerInstance.muted(true);
			}
		},
		publish: function(event, data) {
			data = data || {};
			data.adPlayerName = this.opts.name; // To override current adPlayer name
			data.player = this.player;
			data.playerId = this.opts.id; // We always need the playerId to retrieve it(Logs)
			PubSub.publish(event, data);
		}
	};

	return adPlayer;
})();