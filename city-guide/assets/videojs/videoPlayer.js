var AufVideo = AufVideo || {};
AufVideo.Manager = (function() {
	var self;
	var Manager = function(opts) {
		// Players
		self = this;
		this.labels = opts.labels;
		this.playerType = opts.playerType;
		this.players = [];
		this.playingInstance;
		this.initPlayersListeners();
		
		// Ad players
		this.initAdPlayersListeners();

		// Revolver options
		this.maxCountDown = 6;
		this.revolverRunning = false;

		// Scroll event
		var scrollTimer;
		af_addEvent(document, 'scroll', this.onScroll.bind(this, scrollTimer), false);
	};

	Manager.prototype = {
		init: function(opts) {
			// Initialize ad player
			if (opts.adPlayer && typeof opts.adPlayer.init === 'function') {
				opts.adPlayer.init();
			}

			// Initialize player
			// autoplay from cookbooks
			var done = false;
			var callback = function videoCallback() {
				try {
					if (typeof Mrtn !== 'undefined' && typeof Mrtn.cookbooks !== 'undefined' && Mrtn.cookbooks.cookbooks.Settings) {
						var videoautoplay = Mrtn.cookbooks.cookbooks.Settings.IsVideoAutoPlay;
						if (!videoautoplay) {
							opts.player.opts.setup.autoplay = false;
							opts.player.opts.startOnSight = false;
						}
					}

					opts.player.init();
					self.addPlayer(opts.player);

					// Initialize ad player just after player initialization
					// IMPORTANT: with video-js, some ad players need to be initialized just after and synchronously
					if (opts.adPlayer && typeof opts.adPlayer.onPlayerInit === 'function') {
						opts.adPlayer.onPlayerInit(opts.player);
					}

					// Initialize related contents
					self.initRelatedContents(opts.player);
					done = true;
				}
				catch(e) {
					console.log(e);
				}
			};

			if (typeof Mrtn !== 'undefined' &&
				typeof Mrtn.cookbooks !== 'undefined' &&
				Mrtn.USER) {
				Mrtn.cookbooks.getUserCookbooks( {user:Mrtn.USER, callback:callback});
			}

			if (!done && (typeof Mrtn === 'undefined' || !Mrtn.USER || typeof Mrtn.cookbooks === 'undefined')) {
				callback();
			}
		},
		// Events
		onScroll: function(timer) {
			clearTimeout(timer);
			var that = this;
			timer = setTimeout(function() {
				that.autoplayWhenVisible();
			}, 100);
		},
		// Players
		addPlayer: function(player) {
			this.players.push(player);
		},
		getPlayer: function(playerId) {
			for (var i = 0; i < this.players.length; i++) {
				if (this.players[i].id === playerId) {
					return this.players[i];
				}
			}
			return;
		},
		// Logs
		logError: function(category, data) {
			var player = data.player || this.getPlayer(data.playerId);
			player.logError(data.message, category);
		},
		logEvent: function(message, data) {
			var player = data.player || this.getPlayer(data.playerId);
			player.logEvent(message, data);
		},
		initPlayersListeners: function() {
			var that = this;

			// Log player events
			PubSub.subscribe('video.player.ready', function(msg, data) {
				that.logEvent('player ready', data);

				// Init autoplay for video player when it's ready
				that.autoplayWhenVisible();
			});
			PubSub.subscribe('video.player.play', function(msg, data) {
				that.logEvent('ad opportunities', data);
				that.logEvent('player play', data);
				
				// Automatic pause on scroll
				if (that.playingInstance && data.player.id !== that.playingInstance.id) {
					that.playingInstance.pause(true);
				}

				that.playingInstance = data.player;
			});
			PubSub.subscribe('video.player.complete', function(msg, data) {
				that.logEvent('player complete', data);

				that.revolver(data.player);
			});
			PubSub.subscribe('video.player.revolver', function(msg, data) {
				that.logEvent('player revolver', data);

				af_ua_trackEvent('Video revolver', data.player.getContext(), that.getEventType(), null, true);
				if (data && typeof data.callback === 'function') {
					data.callback();
				}
			});
			
			// Log player errors
			PubSub.subscribe('video.player.setupError', function(msg, data) {
				that.logError('player setup error', data);
			});
			PubSub.subscribe('video.player.error', function(msg, data) {
				that.logError('player error', data);
			});
		},
		// Ad players
		initAdPlayersListeners: function() {
			var that = this;

			PubSub.subscribe('video.ad.player.ready', function(msg, data) {
				that.logEvent('ad player ready', data);

				if (data.adblockDetected) {
					PubSub.publish('video.logger.updateAdPlayerName', data);
				}
			});
			PubSub.subscribe('video.ad.ready', function(msg, data) {
				that.logEvent('ad ' + data.videoBreak + ' ready', data);

				var player = that.getPlayer(data.playerId);
				player.onAdReady({ data: data });
			});
			PubSub.subscribe('video.ad.unfilled', function(msg, data) {
				that.logEvent('ad unfilled', data);
			});
			PubSub.subscribe('video.ad.start', function(msg, data) {
				that.logEvent('ad opportunities', data);
				that.logEvent('ad ' + data.videoBreak + ' start', data);

				var player = that.getPlayer(data.playerId);
				player.autoplayBlocked = true;
				player.pause(true); // As PubSub is async we need to pause again video here
			});
			PubSub.subscribe('video.ad.end', function(msg, data) {
				that.logEvent('ad ' + data.videoBreak + ' end', data);

				var player = that.getPlayer(data.playerId);
				player.autoplayBlocked = false;
				player.play(true); // As PubSub is async we need to play again video here
				player.onAdEnd({ data: data });

				if (data.videoBreak === 'postroll') {
					that.revolver(player);
				}
			});
			PubSub.subscribe('video.ad.validated', function(msg, data) {
				that.logEvent('ad ' + data.videoBreak + ' validated', data);
			});
			PubSub.subscribe('video.ad.notAvailable', function(msg, data) {
				that.logEvent('ad ' + data.videoBreak + ' not available', data);
			});
			PubSub.subscribe('video.ad.block', function(msg, data) {
				that.logEvent('adblock detected', data);
			});
			PubSub.subscribe('video.ad.click', function(msg, data) {
				that.logEvent('ad ' + data.videoBreak + ' click', data);
			});
			PubSub.subscribe('video.ad.impression', function(msg, data) {
				that.logEvent('ad ' + data.videoBreak + ' impression', data);
			});

			// Content
			PubSub.subscribe('video.ad.contentresume', function(msg, data) {
				that.logEvent('ad content resume', data);
			});

			// Errors
			PubSub.subscribe('video.ad.error', function(msg, data) {
				that.logError('ad error', data);
				that.logEvent('ad error', data); // Log ad errors in logmatic to analyse discrepancy
			});
			PubSub.subscribe('video.ad.setupError', function(msg, data) {
				that.logError('ad setup error', data);
			});
		},
		// Related contents
		initRelatedContents: function(player) {
			if (typeof AufVideo.RelatedContents === 'function') {
				var afRelatedContent = new AufVideo.RelatedContents();
				afRelatedContent.init({ 
					labels: this.labels,
					player: player
				});
			}
		},
		// Autoplay when player is 50% viewable
		// Google issue with play/pause => https://developers.google.com/web/updates/2017/06/play-request-was-interrupted
		autoplayWhenVisible: function() {
			for (var i = 0; i < this.players.length; i++) {
				var player = this.players[i];
				var playerContainer = player.getContainer();
				var canPlay = player.hasStartOnSight() && !player.autoplayBlocked;
				var canStick = typeof player.canStick === 'function' && player.canStick();

				if ((canPlay || canStick) && playerContainer) {
					if (af_isInViewport(playerContainer, 0.5)) {
						if (canPlay) {
							if (player.adPlayer && player.adPlayer.opts.noPrefetch && typeof player.adPlayer.requestAds === 'function') {
								player.adPlayer.requestAds();
							}
							player.play(true);
						}

						if (canStick) {
							player.unstick();
						}
					} else {
						if (canStick) {
							player.stick();
						} else if (canPlay) { // Don't pause on stick
							player.pause(true);
						}
					}
				}
			}
		},
		// Revolver: autoplay first related video at the end of the current one
		// TODO: use an other lib than easy pie chart
		revolver: function(player) {
			var revolverData = player.getRevolverData();
			if (!revolverData || !player.isPostrollDone() || !window.location.href.match(/^.*\-n(\d+)\.html(#.*)?$/) || this.revolverRunning) return;

			this.revolverRunning = true;
			var that = this;
			var nextVideoTxt = this.labels.nextVideo.replace('#SECONDS#', '<span id="af_revolver_countdown">0</span>'); 

			var container = document.createElement('div');
			container.setAttribute('id', 'af_nextVideo');
			container.innerHTML = '<a id="af_close_video" class="close-classic"></a>'
								+ '<a id="replay_video">replay</a>'
								+ '<img src="' + revolverData.urlVignette96 + '">'
								+ '<div class="ly"></div>'
								+ '<a class="chart" href="' + revolverData.URLComplete + '">'
								+ '<h2>' + revolverData.titre + '</h2>'
								+ '<span class="playB"></span>'
								+ '<div class="progress" data-percent="100"></div>'
								+ '	<span class="timer">' + nextVideoTxt + '</span>'
								+ '</a>';

			var playerCtn = player.getContainer();
			playerCtn.appendChild(container);

			// Easy Pie Chart
			var charts = [];
			var chartConfig = {
				scaleColor: false,
				trackColor: 'rgba(0, 0, 0, 0)',
				barColor: '#FFFFFF',
				lineWidth: 8,
				lineCap: 'butt',
				size: 150,
				animate: this.maxCountDown * 1000,
				onStop: function() {
					if (that.revolverRunning) {
						PubSub.publish('video.player.revolver', {
							player: player,
							callback: function() {
								location.href = revolverData.URLComplete + '#revolver';
							}
						});
					}
				}
			};
			[].forEach.call(document.getElementsByClassName('progress'), function(el) {
				charts.push(new EasyPieChart(el, chartConfig));
			});

			var closeButton = document.getElementById('af_close_video');
			if (closeButton) {
				af_addEvent(closeButton, 'click', function(e) {
					that.revolverRunning = false;
					af_stopEvent(e);
					that.stopLoader();
					af_remove(document.getElementById('af_nextVideo'));
				}, false);
			}

			var replay_video = document.getElementById('replay_video');
			af_addEvent(replay_video, 'click', function() {
				that.revolverRunning = false;
				that.stopLoader();
				af_remove(document.getElementById('af_nextVideo'));
				player.play(true);
			}, false);

			this.startLoader();
		},
		// TODO: find a better way to retrieve PageType
		getEventType: function() {
			if (window.location.href.indexOf('forum') > -1 || window.location.href.indexOf('foro') > -1) {
				return 'Forum';
			} else {
				return 'Edito';
			}
		},
		startLoader: function() {
			this.initialTime = new Date().getTime();
			this.updateTime();

			if (typeof this.loaderInterval === 'undefined') {
				this.loaderInterval = setInterval(af_bind(this, this.updateTime), 500);
			}
		},
		stopLoader: function() {
			if (typeof this.loaderInterval !== 'undefined') {
				clearInterval(this.loaderInterval);
				this.loaderInterval = undefined;
			}
		},
		updateTime: function() {
			var timeDiff = new Date().getTime() - this.initialTime;
			var countDown = this.maxCountDown - Math.floor(timeDiff / 1e3);
			var countDownElt = document.getElementById('af_revolver_countdown');
			if (countDownElt) {
				if (countDown >= 1) {
					countDownElt.innerHTML = countDown;
				} else {
					countDownElt.classList.add('hidden');
					this.stopLoader();
				}
			} else {
				this.stopLoader();
			}
		}
	};

	// Singleton instance
	var singleInstance;
	return {
		getInstance: function (opts) {
			if (!singleInstance){
				singleInstance = new Manager(opts);
			}
			return singleInstance;
		}
	};
})();