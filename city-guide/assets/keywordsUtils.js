const KeywordsUtils = {
  r: '2r',
  section: '2r',
  sr: '183s',
  subsection: '183s',
  seo_tag: '28278t',
  tag: '122442t%2C28278t', //base de tags
  ops_tag: '122442t',
  story: '2903877s',
  contenttype: 'stories', //base de tags
  site: '1s',
  version: 'mobile',
  platform: 'mobile',

  setKeywords(keywords) {
    window.sas_target = Object.entries(keywords).map(([key, val]) => `${key}=${val}`).join('&')
  },
  
}
module.exports = KeywordsUtils
