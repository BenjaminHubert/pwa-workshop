import siteConfig from "../site.config";

export default async function(store) {
  let endpoint = store.state.graphQLEndPoint
    ? store.state.graphQLEndPoint
    : siteConfig.graphQLEndPoint;

  let messages = await fetch(endpoint, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json"
    },
    body: JSON.stringify({
      query: `{
                locales {
                    Message
                    Key
                }
            }`
    })
  });

  let messagesData = await messages.json();
  let ret = {};

  messagesData.data.locales.forEach(o => {
    ret[o.Key] = o.Message;
  });

  return ret;
}
