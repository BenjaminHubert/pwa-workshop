# Global overview

## PWA

PWA is a buzzword including many things...

- We can define it as native application features now accessible from web browsers (push notification, GPS, shortcut on device home, offline work mode, ...)
- We also can consider it is an **SPA** with **SSR**.

**SPA = Single Page Application**. Example with traditional AngularJS app : everything is done client side with JS. Can be hard to crawl for Google...

**SSR = Server Side Rendering**. The page is rendered server side. Usualy only the first hit is server side rendered then the app works like a classical SPA with JS client side.

At the end, such website are still crawlable from GoogleBot, and UX is similar to AngularJS app, avoiding page reload. And use extended features as in a native application.

Many frameworks help to build such websites: ReactJS, VueJS, ... **we choose VueJS** for his dynamic community, and easy adoption curve.

**Important:** with this technical platform, we focus on the front-end part of websites. Not back-end jobs, neither backoffice interfaces.

### VueJS & NuxtJS

VueJS & NuxtJS are two frameworks, offering the same scope of functionalities than ZendFramework or Symfony.

VueJS organise the front-end part of projects into modules and components.

Every component contains the controller part (JS) and the view part (HTML / CSS / JS).

All the data bring to VueJS is done through API calls. No server-side queries on this part. Please refer to GraphQL for this part :)

https://vuejs.org

NuxtJS organising routing, layout, pages...

It also adds some extra features, including SSR, building the app, etc.

https://nuxtjs.org

### GraphQL

GraphQL is an open source library defining a query language for APIs.
It is a mix between not flexible REST/SOAP APIs, and very flexible SQL requests.

Basicaly, a datamodel is defined (similarly to Looker!) as a mapping to SQL databases, and anyone can "query" one / multiple data over this datamodel.

Benefit is huge: no need to develop specific APIs, and in a single request hit you can get multiple dataset without useless data!

We use the JS implementation of GraphQL, with Express webserver on top of it (running graphQL with NodeJS).

https://graphql.org

## How it works

The main differences between what you know about PHP are:

- We have 2 webservers: 1 for the VueJS code, 1 for the GraphQL data
- We render our data client side (except for the first hit)

Client-side rendering involves passing a JSON object to a template engine that will produce HTML wich will be inject into our DOM.

Here is a workflow schema

![nuxt-views-schema](assets/barcelone/help/pwa-basic-arch.svg)

## Code organisation

Sources are split into 3 repository

### The main template

This repository contain the starter app: a NuxtJS installation with vueJS. your are free to edit what you want as soon as you've copy it to make your own repo.

### The core

The core contains all utilities shared beetween projects like translation, image lazyloading, sticky elements...

It is include into your project via a git subtree.

You should not modify the core directly as it will be overwriting on the next core update. If you want to make modifications please submit a merge request.

### The graphQL server

It is a basic express-graphql server that runs on NodeJS

# Getting started

## Essentials

### Components oriented app

A PWA is usualy a components oriented application, you write isolated components and make them working together.

Component is basicaly a html tag wich accept attributs.

```html
<my-component attr1="value1" attr2="value2" />
```

The component is capable of manipulating attributs to render some html markup and style regarding values.

You already use html components in your everyday code, take a look at these basic html tags, these are all components

```html
<button>
  <input />
  <progress>
    <audio>
      <video>
        ...
      </video>
    </audio></progress
  >
</button>
```

#### VueJS component structure

There are many ways to write Vuejs component but if you have to choose one it will be the Single File Component style aka SFC. You will put in one file your component markup, the associated JS and the style.

```html
<template> </template>

<script>
  export default {};
</script>

<style></style>
```

#### Basic component exemple

```html
<template>
  <button v-on:click="count++">You clicked me {{ count }} times</button>
</template>

<script>
  export default {
    data: function() {
      return {
        count: 0
      };
    }
  };
</script>
<style></style>
```

The first section is the template it's a mixed of well known HTML and VueJS specifiques directive. There is a click event `v-on:click` binded to a simple function that increment the `count` value.

In the script section you put all component logic, here we have declared a variable `count` which is now available in the template as `count` and in the export function as `this.count`.

#### Component essential

here is a typical component structure

```html
<template>
  <div>
    <h1>{{ prop1 }}</h1>
    <!-- props.prop1 value -->
    {{ name }}
    <!-- data.name value -->
  </div>
</template>

<script>
  export default {

  //props are values passed from the component's parent, you can display them in your template.
  props: {
      prop1: "value1"
  }

  //data are typically values you want to display in your template and you want to share throught your component methodes
   data: function () {
     return {
       name: {
           type: String,
           default: ""
       }
     }
   },

   //computed are methods to manipulate your data, values returned by computed method will be cached.
   computed: {
       upperName() {
           return this.name.toUpperCase()
       }
   },

  //methods contains your components methods, unlike computed, values returned by methods are not cached
   methods: {
      async method1() {
          const data = await( await fetch('http://api-url').json());
          
          this.name = data.name;
      }
   }
  }
</script>

<style scoped>
    //your scoped component style
  h1 {
    color: pink;
  }
</style>

<style>
  //unscoped style
</style>
```

Please read about the [official SFC guide](https://vuejs.org/v2/guide/single-file-components.html)

### Nuxt

Nuxt is a framework for VueJS that will handle lot's of [features](https://nuxtjs.org/guide#features) like server side rendering, routing, external ressources loading, middleware etc

## Tutorial

We will do a simple application to show country/capitale maps and details

### Prerequisites

#### Node.js

Make sure you have Node.js installed on your machine using this command :

```bash
node -v
```

<aside class="notice">
Version number should be 10.15+.
</aside>

If you don't, install or update Node.js using n [https://nodejs.org/en/](https://nodejs.org/en/):

```bash
sudo npm install -g n
sudo n stable
```

#### GIT

[https://git-scm.com/downloads](https://git-scm.com/downloads)

## CLI

create a project workspace directory

```bash
cd
mkdir my-new-pwa-workspace-directory
```

go to the newly created project workspace directory

```bash
cd my-new-pwa-workspace-directory
```

clone the following repo

```bash
git clone git@gitlab.com:unifygroup/aufeminin/dev/pwa-cli.git
```

switch branch to barcelone-ssr

```bash
cd pwa-cli
git checkout barcelone-ssr
```

link repo

```bash
npm link
```

### Create new pwa project

go back to your workspace

```bash
cd ..
```

run pwa cli

```bash
pwa
```

Choose `create`, add a project name and choose `no` for `create project on gitlab` and `Install a local database server`

### Starting dev server

go to your fresh pwa project directory

```bash
cd my-fresh-pwa-directory
```

start the server

```bash
npm run dev
```

If all is ok you should be able to browse to http://localhost:3000

Let's start a new app

## The app

We are going to make a simple app that show details about a country/city and his map.

### Map component

A simple component to display an interactive map.
Create a new file under `components` and name it `Map.vue`

We will use [Leaflet](https://leafletjs.com/) to display the map.

#### Load leaflet

First install leaflet, in your root folder type

```bash
npm install leaflet --save
```

To use external lib in Nuxt we have to create a plugin and expose the lib to Vue instance

official plugin documentation (https://nuxtjs.org/guide/plugins/)

Create a file under plugins, name it `vue-leaflet.js`

We import Leaflet and tell Vue tu use it
(`plugins/vue-leaflet.js`)

```js
import Vue from "vue";

import L from "leaflet";
import "leaflet/dist/leaflet.css";

const LeafletPlugin = {
  install(Vue) {
    // Expose Leaflet
    Vue.prototype.$L = L;
  }
};

Vue.use(LeafletPlugin);
```

Now you have to declare your plugin in `nuxt.config.js`, under section plugin (create it if not present) add you plugin.

(`nuxt.config.js`)

```js
const conf = {
  ...,
  plugins: [{ src: "~/plugins/vue-leaflet", ssr: false }],
```

We use the option `ssr: false` to prevent plugin to attempt rendering server side (map can't be rendered server side).

Now we can use Leaflet in our Map component.

#### The template

According to Leaflet documentation the first thing we need is a wrapper for our map, let's declare a div.

(`components/Map.vue`)

```html
<template>
  <div id="map-wrap" />
</template>
```

See official template documentation https://vuejs.org/v2/guide/syntax.html

#### The script

Leaflet require 3 parameters to display a map: longitude, latitude and zoom factor. These params will be passed to our component from his parent so we need to declare them in the props section, remember parent can only access props of childrens.

(`components/Map.vue`)

```html
<template>
  <div id="map-wrap" />
</template>
<script>
  export default {
    //we name the component, usefull for debugging
    name: "LeafletMap",
    //props passed from parent
    props: {
      lat: {
        type: [Number, String],
        default: null
      },
      lng: {
        type: [Number, String],
        default: null
      },
      zoom: {
        type: Number,
        default: 5
      }
    }
  };
</script>
```

Now our component can be called like

```js
<Map :lat="latitudeValue" :lng="longitudeValue" :zoom="zoomFactor" />
```

We now are able to get the 3 needed parameters, let's load the map.

#### Loading map

Leaflet need to have a container for his map, we've already declared one: `<div id="map-wrap" />`, we have to wait for this div to be loaded to be able to call leaflet on it. Component have a [hook](https://vuejs.org/v2/api/#Options-Lifecycle-Hooks) for this, `mounted()`, this hook is called when component's dom is loaded so let's put our leaflet code here.

(`components/Map.vue`)

```html
<script>
  export default {
    //we name the component, usefull for debugging
    name: "LeafletMap",
    //props passed from parent
    props: {
      lat: {
        type: [Number, String],
        default: null
      },
      lng: {
        type: [Number, String],
        default: null
      },
      zoom: {
        type: Number,
        default: 5
      }
    },
    mounted() {
      var mymap = L.map("map-wrap").setView([this.lat, this.lng], this.zoom);
      L.tileLayer("https://{s}.tile.openstreetmap.de/{z}/{x}/{y}.png", {
        attribution:
          'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>'
      }).addTo(mymap);
    }
  };
</script>
```

`setView()` is the main methods to display the map, it needs our 3 parameters to set the right area to show.
To call our props in our script we have to use `this.prop` syntax.

The complete component:
(`components/Map.vue`)

```html
<template>
  <div id="map-wrap" />
</template>

<script>
  export default {
    name: "LeafletMap",
    //props passed from parent
    props: {
      lat: {
        type: [Number, String],
        default: null
      },
      lng: {
        type: [Number, String],
        default: null
      },
      zoom: {
        type: Number,
        default: 5
      }
    },
    mounted() {
      var mymap = L.map("map-wrap").setView([this.lat, this.lng], this.zoom);
      L.tileLayer("https://{s}.tile.openstreetmap.de/{z}/{x}/{y}.png", {
        attribution:
          'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>'
      }).addTo(mymap);
    }
  };
</script>

<style>
  #map-wrap {
    width: 100%;
    height: 300px;
  }
</style>
```

Now we can re-use our Map in other components/pages.

### Country component

Country component will show details and map about a country.

#### The script

This component will need a `countryData` object to display details.
We declare the `countryData` prop typed as Object and set value as `null`

(`components/Country.vue`)

```html
<script>
  export default {
    props: {
      countryData: {
        type: Object,
        default: null
      }
    }
  };
</script>
```

We need to display a map, let's import and register our Map component

(`components/Country.vue`)

```html
<script>
  import CountryMap from "~/components/Map"; // import Map component
  export default {
    props: {
      countryData: {
        type: Object,
        default: null
      }
    },
    components: {
      //register Map component so it can be used in current template
      CountryMap
    }
  };
</script>
```

Map component can now be used in our Country component, just put it in our template.

(`components/Country.vue`)

```html
<template>
  <CountryMap />
</template>

<script>
  import CountryMap from "~/components/Map"; // import Map component
  export default {
    props: {
      countryData: {
        type: Object,
        default: null
      }
    },
    components: {
      //register Map component so it can be used in current template
      CountryMap
    }
  };
</script>
```

#### The country page

To be able to display our Country component we have to register it into a page component.

create a new file under pages, `/pages/country.vue`, and register our Component

(`/pages/country.vue`)

```html
<script>
  import Country from "~/components/Country";

  export default {
    components: {
      Country
    }
  };
</script>
```

Remember the countryData props we declared on our Country component ?
(`components/Country.vue`)

```js
    props: {
      countryData: {
        type: Object,
        default: null
      }
    }
```

we have to create the data object that will contains all data we want to pass to our Country component. Lets create a `dataToCountry` object with some datas

(`/pages/country.vue`)

```html
<script>
  import Country from "~/components/Country";

  export default {
    components: {
      Country
    },
    data() {
      return {
        dataToCountry: {
          name: "Spain",
          longitude: -4,
          latitude: 40
        }
      };
    }
  };
</script>
```

Display Country component and pass him the `dataToCountry` object thought a country-data attribute

> note the use of kebab case for the attribute, as html attributes are not case sensitive you must convert your camel/pascal case props to kebab case !

(`/pages/country.vue`)

```html
<template>
  <!-- countryData is now kebab-case -->
  <Country :country-data="dataToCountry" />
</template>

<script>
  import Country from "~/components/Country";

  export default {
    components: {
      Country
    },
    data() {
      return {
        dataToCountry: {
          name: "Spain",
          longitude: -4,
          latitude: 40
        }
      };
    },
    layout: "main"
  };
</script>
```

We can now test our page in browser, start your server if it's not already done (just run `npm run dev` in the root folder of your project) and browse to http://localhost:3000/country

You should see a nice error message

![nuxt-views-schema](assets/barcelone/help/error.png)

This is because we forgot to pass `lng` and `lat`props to our `MapCountry` component.

Remember our `dataToCountry` object:

(`/pages/country.vue`)

```javascript
dataToCountry: {
  name: "Spain",
  longitude: -4,
  latitude: 40
}
```

We just have to pass longitude and latitude to our map

Let's fix that

(`components/Country.vue`)

```js
<template>
  <CountryMap
    :lng="countryData.longitude"
    :lat="countryData.latitude" />
</template>

<script>
  import CountryMap from "~/components/Map"; // import Map component
  export default {
    props: {
      countryData: {
        type: Object,
        default: null
      }
    },
    components: {
      //register Map component so it can be used in current template
      CountryMap
    }
  };
</script>
```

Back to http://localhost:3000/country, you should now see a map centered on Spain, yeah !

### Data from API

Our countryToData object is static, we want to display map not only for Spain but for any given country.

We will pass the country name in the url and then make a request to https://restcountries.eu/ to get the correct data.

### Routing in NuxtJS

official documentation (https://nuxtjs.org/guide/routing/)

Routing is based on vue-router lib and Nuxt automatically generates the vue-router configuration based on your file tree of Vue files inside the pages directory.

That means that this file tree

```
📦pages
┣ 📜country.vue
┗ 📜help.vue
```

will generate configuration for these routes

- localhost:3000/country
- localhost:3000/help

#### Make dynamic routes

For now we just have a static route to our `country` page but we want to pass params like the country name to our page.

To define a dynamic route with a parameter, you need to define a .vue file OR a directory prefixed by an underscore.

So if we want to pass a param call `name` to our page we will have to rework our file tree.

For now we have

```
📦pages
┗ 📜country.vue
```

and we want a route like `localhost:3000/country/spain`

Our new file tree will look like

```
📦pages
┗ 📂country
┃ ┗ 📜_name.vue
```

`country` became a directory and our old page `country.vue` is now `_name.vue`

Go back to our `_name.vue` wich should look like

(`pages/country/_name.vue`)

```html
<template>
  <Country :country-data="dataToCountry" />
</template>

<script>
  import Country from "~/components/Country";

  export default {
    components: {
      Country
    },
    data() {
      return {
        dataToCountry: {
          name: "Spain",
          longitude: -4,
          latitude: 40
        }
      };
    },
    layout: "main"
  };
</script>
```

Parameters passed to url will be accessible under `$route.params` so to get our name we will have to get the `$route.params.name` value.

Let's assign `this.$route.params.name` to `dataToCountry.name`

(`pages/country/_name.vue`)

```js
...
    data() {
      return {
        dataToCountry: {
          name: this.$route.params.name,
          longitude: -4,
          latitude: 40
        }
      };
    },
    layout: "main"
  };
...
</style>
```

Now we can display our name in Country component

To display data or props in a template just use the mustache syntax {{ }}.
(`components/country`)

```js
<template>
  <div>
    <h1>{{ countryData.name }}</h1>
    <CountryMap
      :lng="countryData.longitude"
      :lat="countryData.latitude" />
  </div>
</template>

<script>
  import CountryMap from "~/components/Map"; // import Map component
  export default {
    props: {
      countryData: {
        type: Object,
        default: null
      }
    },
    components: {
      //register Map component so it can be used in current template
      CountryMap
    }
  };
</script>
```

To display data or props in a template just use the mustache syntax {{ }}.

We had a wrapper `<div>` to our template because nuxt doesn't allow to have more than one root node in a template.

go to http://localhost:3000/country/spain and you'll be able to see our country name above the map.

It's pretty ugly, just put some bootstrap soul in our template

```js
<template>
  <div class="country-card">
    <b-card no-body>
      <CountryMap
        :lng="countryData.longitude"
        :lat="countryData.latitude" />
      <b-card-body>
        <h1>{{ countryData.name }}</h1>
      </b-card-body>
    </b-card>
  </div>
</template>

<script>
  import CountryMap from "~/components/Map"; // import Map component
  export default {
    props: {
      countryData: {
        type: Object,
        default: null
      }
    },
    components: {
      //register Map component so it can be used in current template
      CountryMap
    }
  };
</script>

<style scoped>
  .country-card {
    width: 432px;
  }
</style>
```

Ok better now ! it is time to make our data dynamic.

#### Fetch data from API

First create a `getCountry()` function

(`pages/country/_name.vue`)

```html
...

<script>
  import Country from "~/components/Country";

  async function getCountry(params) {
    const url = `http://restcountries.eu/rest/v2/name/${params.name}`;
    const data = await (await fetch(url)).json();
    return data[0];
  }

  export default {
    components: {
      Country
    },
    data() {
      ...
    },
    layout: "main"
  };
</script>

...
```

To get SSR working we have to call getCountry in the `asyncData` hook of the page.

AsyncData will be merged with data so we can remove our static data object

(`pages/country/_name.vue`)

```html
...

<script>
  import Country from "~/components/Country";

  async function getCountry(params) {
    const url = `http://restcountries.eu/rest/v2/name/${params.name}`;
    const data = await (await fetch(url)).json();
    return data[0];
  }

  export default {
    components: {
      Country
    },
    layout: "main",
    async asyncData({ params, store }) {
      //assign result of getCountry to this.dataToCountry
      return { dataToCountry: await getCountry(params, store) };
    }
  };
</script>

...
```

Browse to http://localhost:3000/country/spain, you should see a new error message `Cannot read property 'lat' of null`, this is because our new country object doesn't have `latitude` and `longitude` keys and we are referencing theses keys in `pages/components/Country.vue`.

```html
<template>
  ...
  <CountryMap :lng="countryData.longitude" :lat="countryData.latitude" />
  ...
</template>
```

In data provided by the API, longitude and latitude are stored like this: `{ latlng: [40, -4] }`, we have to properly extract long and lat values to pass them to our template, vuejs has a mechanisme for that, it's called `computed properties`

Computed [Official documentation](https://vuejs.org/v2/guide/computed.html)

(`components/Country.vue`)

```html
<template>
  <div class="country-card">
    <b-card no-body>
      <CountryMap :lng="longitude" :lat="latitude" />
      <b-card-body>
        <h1>{{ countryData.name }}</h1>
      </b-card-body>
    </b-card>
  </div>
</template>

<script>
  import CountryMap from "~/components/Map"; // import Map component
  export default {
    props: {
      countryData: {
        type: Object,
        default: null
      }
    },
    computed: {
      longitude() {
        if (this.countryData) {
          return this.countryData.latlng[1];
        }

        return null;
      },

      latitude() {
        if (this.countryData) {
          return this.countryData.latlng[0];
        }

        return null;
      }
    },
    components: {
      //register Map component so it can be used in current template
      CountryMap
    }
  };
</script>
```

And we are done, you can test differents url to view associated map:

http://localhost:3000/country/france

http://localhost:3000/country/germany

http://localhost:3000/country/spain

http://localhost:3000/country/cuba

### Go further

- they are many more keys than `name`, `longitude` and `latitude` in datas fetched from restcountries, try to display the country flag , language or population.
- geodb-free-service.wirefreethought.com has got a Capital API, try to make a new page with country's capital details (http://geodb-cities-api.wirefreethought.com/docs/api/find-cities#/)

## Cookbook

### Debugging

Debugging a PWA is like debugging a website, you will rely to your favorite devtools :)

Vue.js devtools : like Angular with batarang or Augury Vue.js has an browser extension to make debugging easier, you will be able to inspect components datas, props or computed in real time.

## Links

- [VuejJS](https://vuejs.org/v2/guide/) (official guide)
  - [Components](https://vuejs.org/v2/guide/components.html)
  - [Template](https://vuejs.org/v2/guide/syntax.html)
  - [Events](https://vuejs.org/v2/guide/events.html)
- [NuxtJS](https://nuxtjs.org/guide/) (official guide)
  - [Routing](https://nuxtjs.org/guide/routing)
  - [Plugins](https://nuxtjs.org/guide/plugins)
  - [Store](https://nuxtjs.org/guide/vuex-store)
- [BootstrapVue](https://bootstrap-vue.js.org/)
- [Leaflet](https://leafletjs.com/)
- [GeoDB API](http://geodb-cities-api.wirefreethought.com/)
- [RestCountry](https://restcountries.eu/)
