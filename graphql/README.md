# graphQL

POC graphQL aufeminin

# Install

### Nodejs

Make sure you have nodejs installed on your machine using this command : 

```bash
node -v
```

version number should be 10.5+.

If you don't install or update nodejs here

[https://nodejs.org/en/](https://nodejs.org/en/)

### GIT

[https://git-scm.com/downloads](https://git-scm.com/downloads)

### Cloning repository

To start your experience with PWA, grab all sources files of the project on your computer. Just copy one of this command in your terminal prompt, then press Enter

via ssh

```bash
$ git clone git@gitlab.com:unifygroup/aufeminin/mobile/aufql.git
```

or via https

```bash
$ git clone https://gitlab.com/unifygroup/aufeminin/mobile/aufql.git
```

## Setting up

```bash
$ cd aufql
$ npm i
```

## Development

### Workflow

#### Make a new local branch

You can't push directly on master, no you can't ! so to start coding a new feature or fixing a bug you will have to make a local branch

```bash
$ git checkout -b my-awesome-new-branch
```

*NB: Please prefix new features with feature-my-awesome-new-feature and bug fix with fix-my-hot-fix*

#### Development server

The common way to start coding on AufQL is to run a local server with hot reload, i.e the server will inject the new code into the page whenever you modify a file.

```bash
$ npm run dev
```

*Server listening on [http://localhost:4000/graphql](http://localhost:4000/graphql)*

You are ready to start coding your awesome feature !

#### Push work to remote

After coding your awesome stuffs, commit you work and push your branch to remote repository to be able to make a merge request

```bash
$ git push --set-upstream origin my-awesome-new-branch
```

#### Merge request

Once you branch is pushed to master you'll be able to make a merge request to master and the deploy you work. Git will show you the direct link to the merge request form, in this exemple: 

[https://gitlab.com/unifygroup/aufeminin/mobile/aufql/merge_requests/new?merge_request%5Bsource_branch%5D=update-readme](https://gitlab.com/unifygroup/aufeminin/mobile/aufql/merge_requests/new?merge_request%5Bsource_branch%5D=update-readme)



```bash
$ git push --set-upstream origin lock-swipe-on-modale
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 8 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 1.62 KiB | 1.62 MiB/s, done.
Total 3 (delta 1), reused 0 (delta 0)
remote: 
remote: To create a merge request for update-readme, visit:
remote:   https://gitlab.com/unifygroup/aufeminin/mobile/aufql/merge_requests/new?merge_request%5Bsource_branch%5D=update-readme
remote: 
To gitlab.com:unifygroup/aufeminin/mobile/aufql.git
 * [new branch]      update-readme -> update-readme
Branch 'update-readme' set up to track remote branch 'update-readme' from 'origin'.You can "cmd + click" on the merge request url provided. 
```

This will open the merge request form on your favorite web browser. On this page:

- Edit the request title if needed
  
  ![](docs/mr-titre.png)
- Check source branch. Should be "your-awesome-new-branch" name
- Check destination branch. Should be "master"
  
  ![](docs/mr-branch.png)
- Check the checkbox "Delete source branch when merge request is accepted" if you want your branch to be deleted on the remote repository.
  
  ![](docs/mr-checkboxes.png)
- Click "Submit merge request" button
  
  ![](docs/mr-button.png)

#### 

#### Deploy to production

One your branch has been lmerged to master you can go to the CI / CD section dans deploy your changes by triggering manual job

![](docs/mr-deploy.png)

# Testing

To run mocha test just run

```bash
$ npm run test
```
