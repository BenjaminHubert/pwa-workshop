import { merge } from "lodash";

const express = require("express");
const express_graphql = require("express-graphql");
const http = require("http");

var fs = require("fs");

const { makeExecutableSchema } = require("graphql-tools");

const Query = `
  type Query {
    _empty: String
  }

  type Mutation {
    _empty: String
  }`;

let typeDefs = [Query]; //initial query required !
let resolversArr = [];

const resolvers = {};

//we will read all resources founded in ./ressources and push their resolvers and typeDef in arrays that will be merged later
fs.readdir("./ressources", function(err, files) {
  if (err) {
    console.error("Could not list the directory.", err);
    process.exit(1);
  }

  files.forEach(function(file, index) {
    typeDefs.push(require(`./ressources/${file}`).typeDef);
    resolversArr.push(require(`./ressources/${file}`).resolvers);
  });

  // Create an express server and a GraphQL endpoint
  var app = express();
  app.use(
    "/graphql",
    (req, res, next) => {
      res.header("Access-Control-Allow-Credentials", true);
      res.header(
        "Access-Control-Allow-Headers",
        "content-type, authorization, content-length, x-requested-with, accept, origin"
      );
      res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
      res.header("Allow", "POST, GET, OPTIONS");
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Cache-Control", "public, s-maxage=2592000");
      if (req.method === "OPTIONS") {
        res.sendStatus(200);
      } else {
        next();
      }
    },
    express_graphql({
      schema: makeExecutableSchema({
        typeDefs,
        resolvers: merge(resolvers, ...resolversArr)
      }),
      graphiql: true
    })
  );

  const httpServer = http.createServer(app);

  httpServer.listen(4000, "0.0.0.0");
  console.log("Server listening on http://localhost:4000/graphql");
});
