const mysql = require("mysql");
const tediousConnection = require("tedious").Connection;
const TediousRequest = require("tedious").Request;
const ESClient = require("@elastic/elasticsearch").Client;
const axios = require("axios");

function doMysql(config, request) {
  const con = mysql.createConnection(config);

  return new Promise((resolve, reject) => {
    con.connect();

    con.query(request, function(error, results, fields) {
      if (error) {
        reject("error connecting mysql");
        console.error("error connecting: " + error);
        return;
      }
      // connected!
      resolve(results.length > 1 ? results : results[0]);
    });

    con.end();
  });
}

function doElasticSearch(config, request) {
  const client = new ESClient(config);
  return client.search(request);
}

function doWebService(config, request) {
  console.log("doRPC", config.endpoint + request);
  return axios.get(config.endpoint + request);
}

function doTedious(config, request) {
  return new Promise((resolve, reject) => {
    let con = new tediousConnection(config);
    let rowsIter = 0;
    con.on("connect", err => {
      // If no error, then good to proceed.
      if (err) {
        console.log("error here", err);
        reject("error");
      }

      let req = new TediousRequest(request, (err, rowCount) => {
        if (err) {
          console.log("error", err);
        }
        if (rowCount < 1) {
          resolve();
        }
        con.close();
      });

      let result = [];

      req.on("row", columns => {
        let photo = {};

        columns.forEach(column => {
          photo[column.metadata.colName] = column.value;
        });
        result.push(photo);
      });

      req.on("done", (rowCount, more, rows) => {
        resolve(result.length > 1 ? result : result[0]);
      });

      con.execSqlBatch(req);
    });
  });
}

export function query(connection, request) {
  if (connection.driver === "mysql") {
    return doMysql(connection.config, request);
  }

  if (connection.driver === "tedious") {
    return doTedious(connection.config, request);
  }

  if (connection.driver === "elasticsearch") {
    return doElasticSearch(connection.config, request);
  }

  if (connection.driver === "webservice") {
    return doWebService(connection.config, request);
  }

  return "wrong connection driver";
}
