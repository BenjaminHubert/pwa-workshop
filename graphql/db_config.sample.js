var config = {};

config.server = process.env.AUFQL_DB_HOST || '127.0.0.1';
config.userName = process.env.AUFQL_DB_USERNAME || 'root';
config.password = process.env.AUFQL_DB_PASSWORD || 'toor';
config.options = {};
config.options.database = process.env.AUFQL_DB_NAME || 'my_aweome_db';
config.options.encrypt = true;

module.exports = config;