require 'json'

namespace :pm2 do

  def app_status
    within current_path do
      ps = JSON.parse(capture :pm2, :jlist, fetch(:app_command))
      if ps.empty?
        return nil
      else
        # status: online, errored, stopped
        return ps[0]["pm2_env"]["status"]
      end
    end
  end

  def restart_app
    within current_path do
      execute :pm2, :restart, fetch(:app_command)
    end
  end

  def start_app
    within current_path do
      execute :pm2, :start, fetch(:app_command)
    end
  end

  def status_app
    within current_path do
      execute :pm2, :status, fetch(:app_command)
    end
  end

  def stop_app
    within current_path do
      execute :pm2, :stop, fetch(:app_command)
    end
  end

  desc 'Stop app'
  task :stop do
    on roles(:node) do
      case app_status
      when nil
        info 'App is not registered'
      when 'stopped'
        info 'App is stopped'
      when 'errored'
        info 'App has errored'
        stop_app
      when 'online'
        info 'App is online'
        stop_app
      end
    end
  end

  desc 'Status app'
  task :status do
    on roles(:node) do
      status_app
    end
  end

  desc 'Start app'
  task :start do
    on roles(:node) do
      case app_status
      when nil
        info 'App is not registered'
        start_app
      when 'stopped'
        info 'App is stopped'
        start_app
      when 'errored'
        error 'App has errored'
      when 'online'
        info 'App is online'
      end
    end
  end

  desc 'Restart app gracefully'
  task :restart do
    on roles(:node) do
      case app_status
      when nil
        info 'App is not registered'
        start_app
      when 'stopped'
        info 'App is stopped'
        restart_app
      when 'errored'
        info 'App has errored'
        restart_app
      when 'online'
        info 'App is online'
        restart_app
      end
    end
  end

end