const q = require("../AufRequest.js");
import { worldstory } from "../db_config.js";

const mysqlConfig = worldstory;

const Queries = {
  GetStory(storyId) {
    return `SELECT S.*
    FROM Story S
    WHERE S.Killed=0 AND S.StoryID = ${storyId}`;
  }
};

export const typeDef = `
extend type Query {
  story(id: Int!): Story,
}

type Story {
  Author: Author
  AuthorID: Int!
  BesoinID: Int!
  Chapo: String!
  ClonedFromStoryVersionID: Int!
  CustomNextId: Int!
  CustomPrevId: Int!
  DateCreation: String!
  DateLimite: String
  DateMiseEnLigne: String
  DateSave: String!
  doValidation: Boolean!
  EnLigne: Boolean!
  IncludeURL: String!
  IncludeURLmobile: String!
  IsGoogleNews: Boolean!
  JSONData: String!
  Killed: Boolean!
  ManageClientID: Int!
  MotsClefsMeta: String!
  MotsClefsURL: String!
  NbComments: Int!
  NbNotes: Int!
  NbSocialComments: Int!
  NoAddLinks: Boolean!
  NoSeeAlso: Boolean!
  NoSocialNetwork: Boolean!
  NoTeasingAuto: Boolean!
  OnlineForReal: Boolean!
  RedirCode: String!
  Rubrique: Rubrique
  RubriqueID: Int!
  Selecteur: String
  SiteID: Int!
  SommeNotes: Int!
  SousRubrique: SousRubrique
  SousRubriqueID: Int
  StoryID: Int!
  StoryVersionID: Int!
  Surtitre: String!
  Tags: [Tag]
  Titre: String!
  TitrePratique: String!
  TitreURL: String!
  TrackingCode: String!
  TrackingSrcImg: String!
  TypeID: Int!
  Template: String
  URLCanonical: String!
  URLComplete: String!
  URLImage: String
  URLImageXL: String
  URLVignette50: String!
  URLVignette96: String!
  withFacebookComment: Boolean!
  MainPicture: StoryPhoto
}`;

function getById(storyId) {
  return q.query(
    {
      driver: "mysql",
      config: mysqlConfig
    },
    Queries.GetStory(storyId)
  );
}

import { getMainPhoto as getMainPicture } from "./storyPhoto.js";
import { getById as getAuthor } from "./author.js";
import { getById as getRubrique } from "./rubrique.js";
import { getById as getSousRubrique } from "./sousRubrique.js";
import { getContent as getTagContent } from "./tag.js";

export const resolvers = {
  Story: {
    Author: story => getAuthor(story.AuthorID),
    Rubrique: story => getRubrique(story.RubriqueID, story.SiteID),
    SousRubrique: story => getSousRubrique(story.SousRubriqueID),
    Tags: story =>
      getTagContent(story.StoryID).then(res => {
        return Array.isArray(res) ? res : [res];
      }),
    MainPicture: story =>
      getMainPicture(story.StoryID).then(res => {
        return res;
      })
  },
  Query: {
    story: (obj, { id }) => {
      return getById(id).then(res => {
        return res;
      });
    }
  }
};
