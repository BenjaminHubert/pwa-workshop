const q = require("../AufRequest.js");
import { marmiton } from "../db_config.js";

const mysqlConfig = marmiton;

const Queries = {
  GetRecipeReview(recipeId, siteID) {
    return `SELECT * FROM review
    where recipe_id = ${recipeId}
    AND killed = false
    AND site_id = ${siteID};`;
  }
};

export const typeDef = `
extend type Query {
  recipeReview(id: Int!): [RecipeReview]
}

type RecipeReview {
  author_name: String
  rating: Int!
  id: Int!
  source: String
  content: String!
  creation_date: String!
}`;

export function getById(rubriqueID, siteID = 13) {
  return q.query(
    {
      driver: "mysql",
      config: mysqlConfig
    },
    Queries.GetRecipeReview(rubriqueID, siteID)
  );
}

export const resolvers = {
  Query: {
    recipeReview: (obj, { id }) => {
      return getById(id);
    }
  }
};
