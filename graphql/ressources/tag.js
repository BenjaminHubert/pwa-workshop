const q = require("../AufRequest.js");
import { worldtag } from "../db_config.js";

const tediousConfig = worldtag;

const Queries = {
  GetTagContent(originId) {
    return `SELECT * FROM TagContent TG WITH(READUNCOMMITTED)
    LEFT JOIN Tag T WITH(READUNCOMMITTED) ON T.TagID = TG.TagID
    WHERE TG.OrigineID = ${originId}`;
  },

  GetTag(tagId) {
    return `SELECT *
      FROM Tag WITH(READUNCOMMITTED)
      WHERE TagID = ${tagId}`;
  },

  GetTagType(tagTypeId) {
    return `SELECT *
      FROM TagType WITH(READUNCOMMITTED)
      WHERE TagTypeID = ${tagTypeId}`;
  }
};

export const typeDef = `
  extend type Query {
    tag(id: Int) : [Tag]
  }

  type Tag {
    TagID: Int!
    TagType: Int!
    TagCleaned: String!
  }`;

export function getContent(originId) {
  return q.query(
    {
      driver: "tedious",
      config: tediousConfig
    },
    Queries.GetTagContent(originId)
  );
}

export function getType(tagTypeId) {
  return q.query(
    {
      driver: "tedious",
      config: tediousConfig
    },
    Queries.GetTagType(tagTypeId)
  );
}

export function getById(tagId) {
  return q.query(
    {
      driver: "tedious",
      config: tediousConfig
    },
    Queries.GetTag(tagId)
  );
}

export const resolvers = {
  Query: {
    tag: (obj, { id }) => {
      return getContent(id).then(res => {
        return res;
      });
    }
  },

  Tag: {
    TagType: tagRes =>
      getById(tagRes.TagID).then(res => {
        return res.TagTypeID;
      })
  }
};
