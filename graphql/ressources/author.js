const q = require("../AufRequest.js");
import { aufemdata } from "../db_config.js";

const tediousConfig = aufemdata;

const Queries = {
  GetAuthor(authorId) {
    return `SELECT Pseudo
    FROM aufemdata.dbo.ManageNom
    where ClientID = ${authorId}`;
  }
};

export const typeDef = `
  extend type Query {
    author(id : Int!): Author
  }

  type Author {
    Pseudo: String!
  }`;

export function getById(authorId) {
  return q.query(
    {
      driver: "tedious",
      config: tediousConfig
    },
    Queries.GetAuthor(authorId)
  );
}

export const resolvers = {
  Query: {
    author: (obj, { id }) => {
      return getById(id).then(res => {
        return res;
      });
    }
  }
};
