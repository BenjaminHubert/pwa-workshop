const q = require("../AufRequest.js");
import { worldstory } from "../db_config.js";

const mysqlConfig = worldstory;

const Queries = {
  GetStoryPhotosFromIds(photoIds) {
    var queryWhere = "";
    var i = 0;
    photoIds.forEach(photo => {
      if (i != 0) {
        queryWhere += " OR ";
      }
      queryWhere = queryWhere + "StoryPhotoID = " + photo;
      i++;
    });

    return `SELECT StoryID, StoryPhotoID, Ordre, DateString, Titre
    FROM StoryPhotos 
    WHERE ${queryWhere}`;
  },

  GetStoryMainPhoto(storyId) {
    return `SELECT * FROM WorldStory.StoryPhotos
    where StoryID = ${storyId}
    order by Ordre ASC LIMIT 1`;
  },

  GetStoryPhotoInfo(photoId) {
    return `SELECT StoryID, StoryPhotoID, Ordre, DateString,
        Titre, width, height, originWidth, originHeight,
        cropX, cropY, cropXTop, cropYTop, cropXBottom, cropYBottom, Credit
        FROM StoryPhotos
        WHERE StoryPhotoID = ${photoId}`;
  },
  GetStoryPhotos(storyId) {
    return `SELECT StoryID, StoryPhotoID, Ordre, DateString,
        Titre, width, height, originWidth, originHeight,
        cropX, cropY, cropXTop, cropYTop, cropXBottom, cropYBottom, Credit, Description
        FROM StoryPhotos SP
        WHERE SP.StoryID=${storyId} 
        ORDER BY SP.DateString DESC`;
  }
};

export function getPhotos(storyId) {
  return q.query(
    {
      driver: "mysql",
      config: mysqlConfig
    },
    Queries.GetStoryPhotos(storyId)
  );
}

export function getMainPhoto(storyId) {
  return q.query(
    {
      driver: "mysql",
      config: mysqlConfig
    },
    Queries.GetStoryMainPhoto(storyId)
  );
}

function getPhotoInfo(photoId) {
  return q.query(
    {
      driver: "mysql",
      config: mysqlConfig
    },
    Queries.GetStoryPhotoInfo(photoId)
  );
}

function getStoryPhotosFromIds(photoIds) {
  return q.query(
    {
      driver: "mysql",
      config: mysqlConfig
    },
    Queries.GetStoryPhotosFromIds(photoIds)
  );
}

export const typeDef = `
  extend type Query {
    storyPhotos(id: Int!): [StoryPhoto],
    storyPhotoInfo(id: Int!): StoryPhoto,
    storyPhotosFromIds(ids: [String!]): [StoryPhoto]
  }

  type StoryPhoto {
    Credit: String
    cropX: Int!
    cropXBottom: Int!
    cropXTop: Int!
    cropY: Int!
    cropYBottom: Int!
    cropYTop: Int!
    DateString: String!
    Description: String
    height: Int!
    Ordre: Int!
    originHeight: Int!
    originWidth: Int!
    StoryID: Int!
    StoryPhotoID: Int!
    Titre: String!
    width: Int!
},`;

export const resolvers = {
  Query: {
    storyPhotos: (obj, { id }) => {
      return getPhotos(id).then(res => {
        return res;
      });
    },
    storyPhotoInfo: (obj, { id }) => {
      return getPhotoInfo(id).then(res => {
        return res;
      });
    },
    storyPhotosFromIds: (obj, { ids }) => {
      return getStoryPhotosFromIds(ids).then(res => {
        return res;
      });
    }
  }
};
