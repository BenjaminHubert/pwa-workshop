const q = require("../AufRequest.js");
import { worlddata } from "../db_config.js";

const tediousConfig = worlddata;

const Queries = {
  GetRubrique(rubriqueID, siteID) {
    return `SELECT *
      FROM SitesRubriques_V1 WITH(READUNCOMMITTED)
      WHERE SiteID = ${siteID}
      AND RubriqueID = ${rubriqueID}`;
  }
};

export const typeDef = `
extend type Query {
  rubrique(id: Int!, siteId: Int!): Rubrique
}

type Rubrique {
  AdPageName: String
  PageID: Int!
  Rubrique: String
  RubriqueID: Int!
}`;

export function getById(rubriqueID, siteID) {
  return q.query(
    {
      driver: "tedious",
      config: tediousConfig
    },
    Queries.GetRubrique(rubriqueID, siteID)
  );
}

export const resolvers = {
  Query: {
    rubrique: (obj, { id, siteId }) => {
      return getById(id, siteId);
    }
  }
};
