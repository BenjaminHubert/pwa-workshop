const q = require("../AufRequest.js");
import { worlddata } from "../db_config.js";

const tediousConfig = worlddata;

const Queries = {
  GetSousRubrique(sousRubriqueID, siteID) {
    return `SELECT *
      FROM SitesSousRubriques_V1 WITH(READUNCOMMITTED)
      WHERE SiteID = ${siteID}
      AND SousRubriqueID = ${sousRubriqueID}`;
  }
};

export const typeDef = `
  extend type Query {
    sousRubrique(id: Int!): SousRubrique
  }

  type SousRubrique {
    Nom: String!
    RubriqueID: Int!
    SousRubriqueID: Int!
  }`;

export function getById(sousRubriqueID, siteID = 1) {
  return q.query(
    {
      driver: "tedious",
      config: tediousConfig
    },
    Queries.GetSousRubrique(sousRubriqueID, siteID)
  );
}

export const resolvers = {
  Query: {
    sousRubrique: (obj, { id }) => {
      return getById(id);
    }
  }
};
