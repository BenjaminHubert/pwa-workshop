const q = require("../AufRequest.js");
import { worldvideo } from "../db_config.js";

const mysqlConfig = worldvideo;

const Queries = {
  GetVideoFromId(videoId) {
    return `SELECT 
      Path,
      VideoLongID,
      VideoID,
      SiteID,
      IsMP4,
      IsMP4HD,
      IsM3U8
      FROM WorldVideo.Video where videoID = ${videoId}`;
  }
};

export const typeDef = `
  extend type Query {
    video(id : Int!) : Video
  }

  type Video {
    IsM3U8: Int
    IsMP4: Int
    IsMP4HD: Int
    Path: String
    SiteID: Int
    VideoID: Int
    VideoLongID: String
  }`;

function getById(videoId) {
  return q.query(
    {
      driver: "mysql",
      config: mysqlConfig
    },
    Queries.GetVideoFromId(videoId)
  );
}

export const resolvers = {
  Query: {
    video: (obj, { id }) => {
      return getById(id).then(res => {
        return res;
      });
    }
  }
};
