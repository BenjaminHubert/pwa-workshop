const q = require("../AufRequest.js");

const tediousConfig = {
  userName: "lgaufemdata",
  password: "kgv%hz4",
  server: "10.2.6.13",
  options: {
    database: "WorldData",
    encrypt: true
  }
};

const Queries = {
  GetSiteFromHost(host) {
    return `SELECT *
      FROM Sites WITH(READUNCOMMITTED)
      WHERE URL = '${host}' AND EnLigne=1`;
  }
};

export const typeDef = `
  extend type Query {
    site(host: String!): Site
  }

  type Site {
    Site: String
    Aff_SiteID: Int!
    CodeLang: String!
  }`;

export function getFromHost(hostname) {
  return q.query(
    {
      driver: "tedious",
      config: tediousConfig
    },
    Queries.GetSiteFromHost(hostname)
  );
}

export const resolvers = {
  Query: {
    site: (obj, { host }) => {
      return getFromHost(host);
    }
  }
};
