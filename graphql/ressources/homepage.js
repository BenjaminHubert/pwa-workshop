const q = require("../AufRequest.js");
import { worldeditoindex } from "../db_config.js";

const tediousConfig = worldeditoindex;

const Queries = {
  GetHomepage(siteId) {
    return `SELECT TOP 30 OrigineID as StoryID, URLComplete, RubriqueID, Titre, C.SiteID
    FROM EditoIndex I WITH (READUNCOMMITTED)
    INNER JOIN EditoIndexContents C WITH (READUNCOMMITTED) ON C.ContentID=I.ContentID
    LEFT JOIN SitesAccrochesOutils SAO WITH (READUNCOMMITTED) ON SAO.EditoIndex_OutilID=C.OutilID
    WHERE C.OutilID = 9 AND C.SiteID = ${siteId}
    ORDER by DateParution desc`;
  }
};

export const typeDef = `
  extend type Query {
    homepageIds(siteId: Int! ): [Story]
  }`;

function getHomepageIds(siteId) {
  return q.query(
    {
      driver: "tedious",
      config: tediousConfig
    },
    Queries.GetHomepage(siteId)
  );
}

export const resolvers = {
  Query: {
    homepageIds: (obj, { siteId }) => getHomepageIds(siteId)
  }
};
