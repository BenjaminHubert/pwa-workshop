const q = require("../AufRequest.js");
import { worldalbum } from "../db_config.js";
const tediousConfig = worldalbum;

const Queries = {
  GetAlbum(albumId, siteId) {
    return `SELECT A.AlbumID, A.SiteID, 
    A.UserID, 
    A.Cle, 
    A.NbPhotos, 
    A.TitreAlbum, 
    A.CommentRights, 
    A.isOnline, 
    A.PublicationDate,
    A.EditoRubriqueID, 
    A.EditoSousRubriqueID, 
    A.EditoURLFolder, 
    A.Killed, 
    A.UserName, 
    A.ManageClientID, 
    A.UrlImagePinterest,
    A.MotDePasse, 
    A.IsLocked, 
    A.VisitesTotales, 
    A.CategorieID, 
    A.DateModif, 
    A.LastModifTimestamp, 
    A.nbComments, 
    A.OrigineID, 
    A.Valide,
    A.DateCreation,
    A.OnlineForReal, 
    A.Valide, 
    P.Hauteur, 
    P.Largeur, 
    CASE WHEN (P.Largeur > P.Hauteur) THEN 1 ELSE 0 END AS isHorizontal,
    (SELECT TOP 1 T.ThematiqueID
    From MonAlbumThematiques T WITH (READUNCOMMITTED)
    WHERE T.AlbumID = A.AlbumID
    ORDER BY T.ThematiqueID DESC) as ThematiqueID
    FROM MonAlbum A WITH (READUNCOMMITTED)
    LEFT JOIN MonAlbumCategoriesLiensRubriques R WITH (READUNCOMMITTED) ON R.SiteID=A.SiteID AND R.CategorieID=A.CategorieID
    LEFT JOIN MonAlbumPhoto P WITH (READUNCOMMITTED) ON A.AlbumID=P.AlbumID AND P.isCover=1 AND P.Largeur<>NULL
    WHERE A.SiteID = ${siteId} AND A.AlbumID = ${albumId} AND A.Killed = 0`;
  },

  GetAlbumPhotos(totalItems, albumId, siteId) {
    return `
        SELECT TOP ${totalItems} P.PhotoID, P.cropXTop, P.cropYTop, P.cropXBottom, P.cropYBottom, P.cropX, P.cropY, P.originHeight, P.originWidth, P.URLVignette, P.AlbumID, P.URLImage, P.Hauteur, P.Largeur, P.Texte, P.Ordre, P.Titre, P.Copyright
        FROM MonAlbumPhoto P WITH (READUNCOMMITTED) 
        INNER JOIN MonAlbum A 
        WITH (READUNCOMMITTED) ON (A.AlbumID = P.AlbumID AND A.Killed = 0)
        WHERE A.SiteID = ${siteId} AND A.AlbumID = ${albumId} AND (P.URLVignette <> '/shim.gif') AND P.Ordre IS NOT NULL
        ORDER BY P.Ordre ASC;`;
  }
};

export const typeDef = `
  extend type Query {
    album(id: Int!, siteId: Int!): Album
    albumPhotos(totalItems: Int!, id: Int!, siteId: Int!): [AlbumPhoto],
  }

  type Album {
    AlbumID: Int! 
    CategorieID: Int! 
    Cle: String! 
    CommentRights: Int! 
    DateCreation: String!
    DateModif: String! 
    EditoPackID: Int! 
    EditoRubriqueID: Int!
    EditoSousRubriqueID: Int! 
    EditoURLFolder: String! 
    Hauteur: Int!
    IncludeURL: String!
    IncludeURLMobile: String!
    IsLocked: Boolean! 
    isOnline: Boolean! 
    Killed: Boolean! 
    Largeur: Int!
    LastModifTimestamp: Int! 
    ManageClientID: Int! 
    MotDePasse: String! 
    nbComments: Int! 
    NbPhotos: Int! 
    OnlineForReal: Boolean!
    OrigineID: Int! 
    PreviewPhotos: [AlbumPhoto]
    PublicationDate: String! 
    RedirectURL: String!
    RightColTeaser: String!
    SeoTitle: String! 
    ShortTitreAlbum: String!
    SiteID: Int! 
    TitreAlbum: String! 
    UrlImagePinterest: String!
    URLKeywords: String!
    UserID: Int! 
    UserName: String! 
    Valide: Boolean!
    VisitesTotales: Int!,
  },
  type AlbumPhoto {
	  cropXTop: Int
	  PhotoID: Int!
    Copyright: String
    cropX: Int
    cropXBottom: Int
    cropY: Int
    cropYBottom: Int
    cropYTop: Int
    originHeight: Int
    originWidth: Int
    Texte: String
    Titre: String
    URLImage: String!
    URLVignette: String!
  }`;

function getById(albumId, siteId) {
  return q.query(
    {
      driver: "tedious",
      config: tediousConfig
    },
    Queries.GetAlbum(albumId, siteId)
  );
}

function getPhotos(totalItems, albumId, siteId) {
  return q.query(
    {
      driver: "tedious",
      config: tediousConfig
    },
    Queries.GetAlbumPhotos(
      totalItems == null ? 30 : totalItems,
      albumId,
      siteId
    )
  );
}

export const resolvers = {
  Query: {
    album: (obj, { id, siteId }) => {
      return getById(id, siteId).then(res => {
        return res;
      });
    },
    albumPhotos: (obj, { totalItems, id, siteId }) => {
      return getPhotos(totalItems, id, siteId).then(res => {
        return res;
      });
    }
  },

  Album: {
    PreviewPhotos: album =>
      getPhotos(6, album.AlbumID, album.SiteID).then(res => {
        return Array.isArray(res) ? res : [res];
      })
  }
};
