const q = require("../AufRequest.js");

const elasticSearchConfig = {
  node:
    "https://search-searchengine-7enjurdboacc6cfsjthncuttwe.eu-west-1.es.amazonaws.com"
};

const webserviceConfig = {
  endpoint: "https://www.aufeminin.com/reloaded/api/search/seealso/story/"
};

const TYPE_STORY_ID = 49;
const TYPE_ALBUM_PHOTO_ID = 28;
const TYPE_VIDEO_ID = 3;

const Queries = {
  GetSeeAlso(storyId, searchText, rubrique, siteName) {
    let query = {
      index: `${siteName}_all_contents`,
      body: {
        from: 0,
        size: 10,
        sort: {
          "all_contents.publicationDate.date.keyword": { order: "desc" }
        },
        query: {
          bool: {
            should: [
              {
                match: {
                  "standard.content": searchText
                }
              },
              {
                match: {
                  "standard.title": searchText
                }
              }
            ],
            filter: {
              bool: {
                should: [
                  {
                    bool: {
                      must: [
                        {
                          term: {
                            "all_contents.section": rubrique
                          }
                        },
                        {
                          term: {
                            "all_contents.online": true
                          }
                        },
                        {
                          terms: {
                            "all_contents.contentTypeId": [TYPE_STORY_ID]
                          }
                        }
                      ]
                    }
                  }
                ]
              }
            },
            minimum_should_match: 1,
            must_not: [
              {
                term: {
                  _id: `${TYPE_STORY_ID}-${storyId}`
                }
              }
            ]
          }
        }
      }
    };
    return query;
  },

  GetSeeAlsoRPC(storyId) {
    return `${storyId}`;
  }
};

export const typeDef = `
  extend type Query {
    seeAlso(storyId: Int!, searchText: String!, rubriqueId: Int!, siteId: Int!, siteName: String!) : [Story]
    seeAlsoRPC(storyId: Int!) : [Story]
  }`;

//direct connection to ES
function getSeeAlso(storyId, searchText, rubriqueId, siteName) {
  return q.query(
    {
      driver: "elasticsearch",
      config: elasticSearchConfig
    },
    Queries.GetSeeAlso(storyId, searchText, rubriqueId, siteName)
  );
}

//use aufeminin webservice
function getSeeAlsoRPC(storyId) {
  return q.query(
    {
      driver: "webservice",
      config: webserviceConfig
    },
    Queries.GetSeeAlsoRPC(storyId)
  );
}

export const resolvers = {
  Query: {
    seeAlso: (obj, { storyId, searchText, rubriqueId, siteId, siteName }) => {
      return getSeeAlso(storyId, searchText, rubriqueId, siteName).then(res =>
        res.body.hits.hits.map(hit => {
          return {
            Titre: hit._source.standard.title,
            StoryID: hit._id.split("-")[1],
            RubriqueID: rubriqueId,
            SiteID: siteId,
            URLImage: hit._source.display.url_image,
            URLComplete: hit._source.display.url,
            AuthorID: hit._source.all_contents.authorId,
            DateMiseEnLigne: hit._source.all_contents.publicationDate.date
          };
        })
      );
    },

    seeAlsoRPC: (obj, { storyId }) => {
      return getSeeAlsoRPC(storyId).then(res =>
        res.data.links.map(hit => {
          return {
            Titre: hit.title,
            StoryID: hit.id,
            URLImage: hit.URLImageMax,
            URLComplete: hit.url
          };
        })
      );
    }
  }
};
