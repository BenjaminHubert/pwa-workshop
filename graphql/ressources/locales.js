const q = require("../AufRequest.js");
var md5 = require("md5");

const tediousConfig = {
  userName: "lgaufemdata",
  password: "kgv%hz4",
  server: "10.2.6.13",
  options: {
    database: "WorldData",
    encrypt: true
  }
};

export const typeDef = `
  extend type Query {
    translations(context: String!, siteId: Int!): [Translation]
    allTranslations(siteId: Int!) : [Translation]
  }

  type Translation {
    Sentence: String!
    Context: String!
    Translation: String!
  }
  
  extend type Mutation {
    setTranslation(context: String!, siteId: Int!, sentence: String!): String
  }`;

const Queries = {
  GetTranslationsFromContext(context, siteId) {
    return `SELECT Validated, Sentence
              FROM TranslationPhp WITH (READUNCOMMITTED)
              WHERE Context = '${context}'
        			AND SiteID = ${siteId}
              AND Killed = 0;`;
  },

  GetAll(siteId) {
    return `SELECT Validated, Sentence, Context, Translation
              FROM [WorldData].[dbo].[TranslationPhp] WITH (READUNCOMMITTED)
              WHERE SiteID = ${siteId}
              AND Killed = 0 AND Context NOT LIKE '%Manage%' AND Context != '';`;
  },

  InsertTranslation(context, siteId, sentence) {
    let sentenceMd5 = md5(sentence);

    return `INSERT INTO TranslationPhp WITH (UPDLOCK)
             		(SentenceMd5, Context, SiteID, Sentence)
          		  VALUES ('${sentenceMd5}', '${context}', ${siteId}, '${sentence}')`;
  },

  GetBySentence(context, siteId, sentence) {
    return `SELECT Translation, Validated, Killed
              FROM TranslationPhp WITH (READUNCOMMITTED)
              WHERE SentenceMd5 = '${md5(sentence)}'
        		    AND Context = '${context}'
        		    AND SiteID = ${siteId}`;
  },

  ResurrectTranslation(context, siteId, sentence) {
    return `UPDATE TranslationPhp WITH (UPDLOCK)
              SET Killed = 0,
              LastUsage = GETDATE()
              WHERE SentenceMd5 = '${md5(sentence)}'
                AND Context = '${context}'
                AND SiteID = ${siteId}`;
  }
};

function resurrectTranslation(context, siteId, sentence) {
  return q.query(
    {
      driver: "tedious",
      config: tediousConfig
    },
    Queries.ResurrectTranslation(context, siteId, sentence)
  );
}

function getByContext(context, siteId) {
  return q.query(
    {
      driver: "tedious",
      config: tediousConfig
    },
    Queries.GetTranslationsFromContext(context, siteId)
  );
}

function getAll(siteId) {
  return q.query(
    {
      driver: "tedious",
      config: tediousConfig
    },
    Queries.GetAll(siteId)
  );
}

function getBySentence(context, siteId, sentence) {
  return q.query(
    {
      driver: "tedious",
      config: tediousConfig
    },
    Queries.GetBySentence(context, siteId, sentence)
  );
}

function insertTranslation(context, siteId, sentence) {
  return q.query(
    {
      driver: "tedious",
      config: tediousConfig
    },
    Queries.InsertTranslation(context, siteId, sentence)
  );
}

export const resolvers = {
  Query: {
    translations: (obj, { context, siteId }) => {
      return getByContext(context, siteId);
    },
    allTranslations: (obj, { siteId }) => {
      return getAll(siteId);
    }
  },

  Mutation: {
    setTranslation: async (obj, { context, siteId, sentence }) => {
      let res = await getBySentence(context, siteId, sentence);
      if (res && res.Killed) {
        return resurrectTranslation(context, siteId, sentence);
      } else {
        return insertTranslation(context, siteId, sentence);
      }
    }
  }
};
