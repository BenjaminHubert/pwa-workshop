"use strict";

const chai = require("chai");
const expect = chai.expect;
const doQuery = require("./utils.js").doQuery;
require('./test_conf.js');

describe("SousRubrique", () => {
  it("Should get correct SousRubrique from id 156", () => {
    const query = `{ sousRubrique(id:156) { Nom RubriqueID } }`;
    const expected = {
      sousRubrique: {
        Nom: "News société",
        RubriqueID: 26
      }
    };

    return doQuery(query)
      .then(res => res.json())
      .then(res => {
        expect(res.data).to.have.deep.equals(expected);
      });
  });
});
