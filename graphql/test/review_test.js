"use strict";

const chai = require("chai");
const expect = chai.expect;
const doQuery = require("./utils.js").doQuery;
require('./test_conf.js');

describe("RecipeReview", () => {
  it("Should get some recipe reviews from recipe 22726", () => {
    const query = `{ 
        recipeReview(id: 22726) { author_name rating creation_date source id}
    }`;

    return doQuery(query)
    .then(res => res.json())
    .then(res => {
        expect(res.data.recipeReview).to.be.an('array');
        expect(res.data.recipeReview[0]).to.have.all.keys('author_name', 'rating', 'creation_date', 'source', 'id');
    });
  })
})