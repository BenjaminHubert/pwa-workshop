"use strict";

const chai = require("chai");
const expect = chai.expect;
const doQuery = require("./utils.js").doQuery;
require("./test_conf.js");

describe("Rubrique", () => {
  it("Should get correct rubrique from id 26", () => {
    const query = `{ rubrique(id:26, siteId:1) { Rubrique }}`;
    const expected = {
      rubrique: {
        Rubrique: "Société"
      }
    };

    return doQuery(query)
      .then(res => res.json())
      .then(res => {
        expect(res.data).to.have.deep.equals(expected);
      });
  });
});
