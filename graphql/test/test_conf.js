const iserv = require("./integration");
let app; 

beforeEach((done) => {
 app = iserv.start(done);
});

afterEach((done) => {
  iserv.stop(app, done);
});
