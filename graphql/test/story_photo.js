"use strict";

const chai = require("chai");
const expect = chai.expect;
const doQuery = require('./utils.js').doQuery;
require('./test_conf.js');


describe("StoryPhotos", () => {
    it("Should get correct photo collection from story 3041807", () => {
      const query = `{storyPhotos (id:3041807){ StoryID }}`;
      const expected = {
        storyPhotos: [
          {
            StoryID: 3041807
          },
          {
            StoryID: 3041807
          }
        ]
      };
  
      return doQuery(query)
        .then(res => res.json())
        .then(res => {
          expect(res.data).to.have.deep.equals(expected);
        });
    });
  });