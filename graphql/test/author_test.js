"use strict";

const chai = require("chai");
const expect = chai.expect;
const doQuery = require("./utils.js").doQuery;
require('./test_conf.js');

describe("Author", () => {
  it("Should get correct author for id 1962", () => {
    const query = `{ author(id:1962) { Pseudo }}`;

    const expected = {
      author: {
        Pseudo: "Mathilde Wattecamps"
      }
    };

    return doQuery(query)
      .then(res => res.json())
      .then(res => {
        expect(res.data).to.have.deep.equals(expected);
      });
  });
});
