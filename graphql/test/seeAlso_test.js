"use strict";

const chai = require("chai");
const expect = chai.expect;
const doQuery = require("./utils.js").doQuery;
require("./test_conf.js");

describe("SeeAlso", () => {
  it("Should get some see also from rubrique 26", () => {
    const query = `{
      seeAlso(rubriqueId:26, siteName:"aufeminin", storyId:3066765, siteId:1 ,searchText:" facteur  prend  photo   chiens qu\u0027 croise    met  baume  coeur   Nouvelle Orléans,  facteur  pris  habitude   prendre  photo    chiens qu\u0027 croise   route. \u0027  moment douceur   journée.") {
      Titre StoryID RubriqueID Rubrique { Rubrique } URLImage URLComplete
      }
    }`;

    return doQuery(query)
      .then(res => res.json())
      .then(res => {
        expect(res.data.seeAlso).to.be.an("array");
        expect(res.data.seeAlso[0]).to.have.all.keys(
          "StoryID",
          "RubriqueID",
          "Rubrique",
          "Titre",
          "URLImage",
          "URLComplete"
        );
        expect(res.data.seeAlso[0].Rubrique).to.have.all.keys("Rubrique");
      });
  });
});
