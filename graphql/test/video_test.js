"use strict";

const chai = require("chai");
const expect = chai.expect;
const doQuery = require("./utils.js").doQuery;

describe("Video", () => {
  it("Should get correct rubrique from id 26", () => {
    const query = `{ video(id:619594) { IsM3U8 IsMP4 IsMP4HD Path SiteID VideoID VideoLongID }}`;
    const expected = {
        "video": {
            "IsM3U8": 1,
            "IsMP4": 1,
            "IsMP4HD": 1,
            "Path": "20190130",
            "SiteID": 1,
            "VideoID": 619594,
            "VideoLongID": "v_619594"
          }
    };

    return doQuery(query)
    .then(res => res.json())
    .then(res => {
      expect(res.data).to.have.deep.equals(expected);
    });
  });
});
