const fetch = require("node-fetch");

module.exports = {
  doQuery: function(query) {
    return fetch("http://localhost:4000/graphql", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ query })
    });
  }
};
