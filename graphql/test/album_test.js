"use strict";

const chai = require("chai");
const expect = chai.expect;
const doQuery = require("./utils.js").doQuery;
require("./test_conf.js");

describe("Album", () => {
  it("Should get correct album from id 1330755", () => {
    const query = `{ album(id: 1330755, siteId: 1) { AlbumID TitreAlbum NbPhotos PreviewPhotos { URLImage cropX cropY originHeight originWidth Texte } } }`;
    // const expected = {
    //   album: {
    //     AlbumID: 1330755,
    //     TitreAlbum:
    //       "Simone Veil : retour sur les événements marquants de sa vie",
    //     NbPhotos: 30,
    //     PreviewPhotos: [
    //       {
    //         URLImage:
    //           "/D20180626/simone-veil-retour-sur-les-evenements-marquants-de-sa-vie-phalbm25579755.jpg",
    //         cropX: 0,
    //         cropY: 0,
    //         originHeight: 1257,
    //         originWidth: 1257,
    //         Texte:
    //           "<p><b>Simone Veil </b>fera son entr&#233;e au panth&#233;on ce <b>dimanche 1er juillet 2018,</b> accompagn&#233;e de son mari Antoine Veil. Elle est la quatri&#232;me femme &#224; recevoir cet hommage.</p><p>Simone Veil et son mari reposeront dans la<b> crypte du Panth&#233;on.</b><span><span class=\"Apple-converted-space\">&#160;Ils seront</span></span><span><b>&#160;</b>dans la m&#234;me aile que les r&#233;sistants Jean Moulin et Pierre Brossolette, le fondateur de l'Europe Jean Monnet, et l'&#233;crivain Andr&#233; Malraux.</span></p><p><span><br/></span></p><p><span><b>Simone Veil </b>a r&#233;alis&#233; une carri&#232;re professionnelle aussi exemplaire qu&#8217;exceptionnelle.&#160;</span></p><p><span>En 1970, elle devient la <b>premi&#232;re femme secr&#233;taire g&#233;n&#233;rale du Conseil sup&#233;rieur de la magistrature. </b><br/>En 1974, elle est nomm&#233;e <b>ministre de la Sant&#233;</b> p</span><span>ar le pr&#233;sident Val&#233;ry Giscard d&#8217;Estaing. Gr&#226;ce &#224; ses nouvelles fonctions, elle fait passer la loi pour la l&#233;galisation de l&#8217;avortement en 1975, baptis&#233;e Loi Veil. <br/>En 1979, elle devient la <b>premi&#232;re femme pr&#233;sidente du Parlement europ&#233;en,</b> avant d&#8217;acc&#233;der au poste de ministre des Affaires sociales et de la Sant&#233;, dans le gouvernement d&#8217;Edouard Balladur. <br/>Enfin, elle est &#233;lue &#224; l<b>&#8217;Acad&#233;mie fran&#231;aise en 2008,</b> apr&#232;s avoir si&#233;g&#233; neuf ans au Conseil constitutionnel (1998-2007).&#160;</span></p><p><span>Gr&#226;ce &#224; ses nombreux combats, Simone Veil a ouvert la voie &#224; toutes les&#160;femmes politiques</span><span>&#160;</span><span>qui lui ont succ&#233;d&#233;.&#160;</span><br/></p><p><span><br/></span></p><p><span><b>Retour sur les moments qui ont marqu&#233; la vie de Simone Veil</b></span></p>"
    //       },
    //       {
    //         URLImage:
    //           "/D20180627/simone-veil-retour-sur-les-evenements-marquants-de-sa-vie-phalbm25580027.jpg",
    //         cropX: 0,
    //         cropY: 0,
    //         originHeight: 5334,
    //         originWidth: 3552,
    //         Texte:
    //           "<p>Simone Jacob est n&#233;e le 13 juillet 1927 &#224; Nice.</p>"
    //       },
    //       {
    //         URLImage:
    //           "/D20180626/simone-veil-retour-sur-les-evenements-marquants-de-sa-vie-phalbm25579600.jpg",
    //         cropX: 0,
    //         cropY: 0,
    //         originHeight: 3661,
    //         originWidth: 2433,
    //         Texte:
    //           "<p>A l'&#226;ge de 16 ans, en 1944, elle est arr&#234;t&#233;e par deux SS &#224; Nice<br/></p>"
    //       },
    //       {
    //         URLImage:
    //           "/D20180626/simone-veil-retour-sur-les-evenements-marquants-de-sa-vie-phalbm25579601.jpg",
    //         cropX: 0,
    //         cropY: 0,
    //         originHeight: 3224,
    //         originWidth: 4836,
    //         Texte:
    //           "<p>Le 15 avril 1944, Simone Jacob est d&#233;port&#233;e vers le camp d'extermination Auschwitz avec sa m&#232;re et sa soeur</p>"
    //       },
    //       {
    //         URLImage:
    //           "/D20180626/simone-veil-retour-sur-les-evenements-marquants-de-sa-vie-phalbm25579622.jpg",
    //         cropX: 0,
    //         cropY: 0,
    //         originHeight: 4240,
    //         originWidth: 3372,
    //         Texte:
    //           "<p>Le camp de Bergen-Belsen est lib&#233;r&#233; par les troupes britanniques le 15 avril 1945</p>"
    //       },
    //       {
    //         URLImage:
    //           "/D20180626/simone-veil-retour-sur-les-evenements-marquants-de-sa-vie-phalbm25579599.jpg",
    //         cropX: 0,
    //         cropY: 0,
    //         originHeight: 4711,
    //         originWidth: 7037,
    //         Texte:
    //           "<p>Simone Jacob &#233;pouse Antoine Veil en 1946. Ils se sont rencontr&#233;s &#224; Sciences-Po Paris et ont eu trois enfants.</p>"
    //       }
    //     ]
    //   }
    // };

    return doQuery(query)
      .then(res => res.json())
      .then(res => {
        // expect(res.data).to.have.deep.equals(expected);
        expect(res.data.album).to.have.all.keys(
          "AlbumID",
          "TitreAlbum",
          "NbPhotos",
          "PreviewPhotos"
        );

        expect(res.data.album.PreviewPhotos[0]).to.have.all.keys(
          "URLImage",
          "cropX",
          "cropY",
          "originHeight",
          "originWidth",
          "Texte"
        );
      });
  });
});
