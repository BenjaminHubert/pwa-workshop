"use strict";
import { merge } from "lodash";
const http = require("http");

var fs = require("fs");
const express = require("express");
const express_graphql = require("express-graphql");

const { makeExecutableSchema } = require("graphql-tools");

const Query = `
type Query {
  _empty: String
}

  type Mutation {
    _empty: String
  }`;

let typeDefs = [Query]; //initial query required !
let resolversArr = [];

const resolvers = {};

function start(done, appPort) {
  const app = express();
  app.use("/graphql", (req, res, next) => {
    if (req.method === "OPTIONS") {
      res.sendStatus(200);
    } else {
      next();
    }
  });
  const httpServer = http.createServer(app);

  fs.readdir("./ressources", function(err, files) {
    if (err) {
      console.error("Could not list the directory.", err);
      process.exit(1);
    }

    files.forEach(function(file, index) {
      typeDefs.push(require(`../ressources/${file}`).typeDef);
      resolversArr.push(require(`../ressources/${file}`).resolvers);
    });

    // Create an express server and a GraphQL endpoint
    app.use(
      express_graphql({
        schema: makeExecutableSchema({
          typeDefs,
          resolvers: merge(resolvers, ...resolversArr)
        }),
        graphiql: true
      })
    );

    httpServer.listen(4000, () => {
      done();
    });
  });

  return httpServer;
}

function stop(app, done) {
  app.close();
  done();
}

module.exports = {
  start,
  stop
};
