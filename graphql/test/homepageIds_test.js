"use strict";

const chai = require("chai");
const expect = chai.expect;
const doQuery = require("./utils.js").doQuery;
require("./test_conf.js");

describe("HomepageIds", () => {
  it("Should get { StoryID } collection", () => {
    const query = `{homepageIds(siteId:1) { StoryID }}`;

    return doQuery(query)
      .then(res => res.json())
      .then(res => {
        expect(res.data.homepageIds).to.be.an("array");
        expect(res.data.homepageIds[0]).to.have.all.keys("StoryID");
      });
  });
});
