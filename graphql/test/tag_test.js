"use strict";

const chai = require("chai");
const expect = chai.expect;
const doQuery = require("./utils.js").doQuery;
require("./test_conf.js");

describe("Tags", () => {
  it("Should get correct Tags from story  3041608", () => {
    const query = "{ story(id: 3041608) { Tags { TagType TagID TagCleaned } }}";
    const expected = {
      story: {
        Tags: [
          {
            TagType: 1,
            TagID: 19586,
            TagCleaned: "Football"
          },
          {
            TagType: 1,
            TagID: 19501,
            TagCleaned: "Sport"
          },
          {
            TagType: 1,
            TagID: 19649,
            TagCleaned: "Football-feminin"
          },
          {
            TagType: 1,
            TagID: 123183,
            TagCleaned: "Journee-des-droits-des-femmes"
          },
          {
            TagType: 1,
            TagID: 29368,
            TagCleaned: "activite-sportive"
          }
        ]
      }
    };
    return doQuery(query)
      .then(res => res.json())
      .then(res => {
        expect(res.data.story.Tags[0]).to.have.all.keys(
          "TagType",
          "TagID",
          "TagCleaned"
        );
      });
  });
});
