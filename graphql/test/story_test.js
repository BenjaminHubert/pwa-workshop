"use strict";

const chai = require("chai");
const expect = chai.expect;
const doQuery = require("./utils.js").doQuery;
require("./test_conf.js");

describe("Story", () => {
  it("Should get correct story structure", () => {
    const query = `{ story(id: 3041608) { Titre Chapo JSONData URLImage StoryID DateMiseEnLigne Tags { TagID TagType TagCleaned } URLImageXL RubriqueID SousRubriqueID Rubrique { Rubrique AdPageName PageID } Author { Pseudo } SousRubrique { Nom } }}`;

    return doQuery(query)
      .then(res => res.json())
      .then(res => {
        // expect(res.data).to.have.deep.equals(expected);
        expect(res.data.story).to.have.all.keys(
          "Titre",
          "Chapo",
          "JSONData",
          "URLImage",
          "StoryID",
          "DateMiseEnLigne",
          "Tags",
          "URLImageXL",
          "RubriqueID",
          "SousRubriqueID",
          "Rubrique",
          "Author",
          "SousRubrique"
        );

        expect(res.data.story.Tags[0]).to.have.all.keys(
          "TagID",
          "TagType",
          "TagCleaned"
        );

        expect(res.data.story.Rubrique).to.have.all.keys(
          "Rubrique",
          "AdPageName",
          "PageID"
        );

        expect(res.data.story.Author).to.have.all.keys("Pseudo");
        expect(res.data.story.SousRubrique).to.have.all.keys("Nom");
      });
  });
});
