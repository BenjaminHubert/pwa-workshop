# server-based syntax
# ======================
# Defines a single server with a list of roles and multiple properties.
# You can define all roles on a single server, or split them:

server '10.2.1.87', roles: %w[aufql node]
server '10.2.1.88', roles: %w[aufql node]

set :ssh_options,
    user: 'claptrap',
    keys: %w[~/.ssh/deploy_af],
    forward_agent: true,
    auth_methods: %w[publickey]
