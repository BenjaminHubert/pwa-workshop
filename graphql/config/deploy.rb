# config valid for current version and patch releases of Capistrano
lock '~> 3.11.0'

set :application, 'aufql'
set :app_command, 'pm2.yml'
set :repo_url, 'git@gitlab.com:unifygroup/aufeminin/mobile/aufql.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp
set :branch, ENV['CI_COMMIT_SHA'] if ENV['CI_COMMIT_SHA']

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/app/aufeminin/aufql'
set :npm_flags, '--production --silent --no-progress'

# Default value for :linked_files is []
append :linked_files, 'db_config.js', 'pm2.yml'

# Default value for linked_dirs is []
append :linked_dirs, 'node_modules'

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Default value for keep_releases is 5
# set :keep_releases, 5

# Uncomment the following to require manually verifying the host key before first deploy.
# set :ssh_options, verify_host_key: :secure

namespace :deploy do
  desc 'Restart application'
  task :restart do
    invoke 'pm2:restart'
  end

  after :publishing, :restart
end
